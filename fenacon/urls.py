from django.conf.urls import patterns, include, url
from django.contrib import admin
from apps.flatpages.views import (ActingDetailView, InstitutionalDetailView,
                                  AnnounceView)
from apps.office.views import SyndicateListView, OfficeListView
from apps.website.views import HomeView, SearchView
from apps.website.api import (api_root, NewsListAPI, NewsCategoryListAPI,
                              EventListAPI, PressClippingListAPI,
                              CreateUserAPI, FenaconMidiaListAPI,
                              SyndicateListAPI, OfficeListAPI, AboutUsListAPI,
                              ObtainAuthToken, MagazineListAPI,
                              PressClippingAPI, NewsDetailAPI)
from fenacon import settings

urlpatterns = patterns('',
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^', include('apps.website.urls')),
    url(r'^multimidia/', include('apps.multimidia.urls')),
    url(r'^busca/', SearchView.as_view(), name='search'),
    url(r'^sindicatos-filiados/', SyndicateListView.as_view(), name='syndicate'),
    # url(r'^escritorios/', OfficeListView.as_view(), name='offices'),
    url(r'^atuacao/(?P<slug>[\w_-]+)-(?P<pk>\d+)/', ActingDetailView.as_view(), name='acting_detail'),
    url(r'^institucional/(?P<slug>[\w_-]+)-(?P<pk>\d+)/', InstitutionalDetailView.as_view(), name='institutional_detail'),
    url(r'^publicacoes/(?P<slug>[\w_-]+)/', AnnounceView.as_view(), name='announce_detail'),

    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += [
    url(r'^api/$', api_root),
    url(r'^api/token-auth/', ObtainAuthToken.as_view()),
    url(r'^api/news/$', NewsListAPI.as_view(), name='news_list_api'),
    url(r'^api/news/(?P<pk>\d+)/?$', NewsDetailAPI.as_view(),
        name='news_detail_api'),
    url(r'^api/events/$', EventListAPI.as_view(), name='event_list_api'),
    url(r'^api/category/$', NewsCategoryListAPI.as_view(),
        name='category_list_api'),
    url(r'^api/user/create/$', CreateUserAPI.as_view(),
        name='user_create_api'),
    url(r'^api/press-clipping/$', PressClippingListAPI.as_view(),
        name='pressclipping_list_api'),
    url(r'^api/press-clipping/(?P<pk>\d+)/?$', PressClippingAPI.as_view(),
        name='pressclipping_detail_api'),
    url(r'^api/fenacon-midia/$', FenaconMidiaListAPI.as_view(),
        name='fenaconmidia_list_api'),
    url(r'^api/syndicate/$', SyndicateListAPI.as_view(),
        name='syndicate_list_api'),
    url(r'^api/office/$', OfficeListAPI.as_view(),
        name='office_list_api'),
    url(r'^api/about-us/$', AboutUsListAPI.as_view(),
        name='aboutus_list_api'),
    url(r'^api/magazine/$', MagazineListAPI.as_view(),
        name='magazine_list_api'),
]

urlpatterns += patterns('',
    url(r'^media/(.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    url(r'^static/(.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    (r'^ckeditor/', include('ckeditor_uploader.urls')),
)

admin.site.site_header = 'FENACON Website'