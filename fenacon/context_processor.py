from apps.flatpages.models import Institutional, Acting, Announce
from apps.website.models import SocialNetwork, NewsCategory


def statics(request):
    institutional = Institutional.objects.all()
    acting = Acting.objects.all()
    news_categories = NewsCategory.objects.all()
    return_dict = {'institutional_list': institutional, 'acting_list': acting, 'news_categories': news_categories}
    try:
        social_networks = SocialNetwork.objects.all()[0]
        return_dict['contact'] = social_networks
    except:
        pass
    try:
        announce = Announce.objects.all()[0]
        return_dict['announce'] = announce
    except:
        pass
    return return_dict
