// NOTA IMPORTANTE: esse arquivo est� no s3, se for alterado � preciso subir ele manualmente

var _form;
var _boxSuccessMsg;
var _btn;
var _timer;
var _doc = this;
var counterId=0;
var xmlHttp;

//AJAX
postRequest = function postRequest(formData, uniqueId, btn, boxSuccessMsg){
    if(!btn) btn=_btn;
    if(!boxSuccessMsg) boxSuccessMsg = _boxSuccessMsg;

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open('POST', 'http://aknapps.com/emkt/listas/index.php', true);
    xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xmlHttp.send(formData);

    xmlHttp.onreadystatechange = function(){
        if (xmlHttp.readyState == 4){
            var msg = JSON.parse(xmlHttp.responseText).message;
            var isError = xmlHttp.status!=200;

            if (msg != null && msg.length > 0){
                requestResponse(isError ? 'status-error' : 'status-success',msg, boxSuccessMsg);
                return;
            }

            requestResponse('status-error','Erro. Tente mais tarde');
        }

        btn.disabled=false;

        clearTimer(uniqueId, boxSuccessMsg);
    }
};

function requestResponse(classAlert,textMessage, boxSuccessMsg, form){
    if(!boxSuccessMsg) boxSuccessMsg = _boxSuccessMsg;
    if(!form) form = _form;

    if(boxSuccessMsg.getAttribute('color')!=null){
        boxSuccessMsg.setAttribute('color', classAlert=='status-error' ? 'red' : 'green');
    }
    else{
        boxSuccessMsg.className = classAlert;

        boxSuccessMsg.style.width = form.getElementsByTagName('button')[0].offsetLeft+form.getElementsByTagName('button')[0].offsetWidth+1;
    }

    boxSuccessMsg.innerHTML =textMessage;
};

function clearTimer(uniqueId, boxSuccessMsg){
    if(uniqueId){
        clearTimeout(_doc["timer"+uniqueId]);
        _doc["timer"+uniqueId] = setInterval(function(){
            requestResponse('', '', boxSuccessMsg);
            clearTimeout(_doc["timer"+uniqueId]);
        }, 7000);
    }else{
        clearTimeout(_timer);
        _timer = setInterval(function(){
            requestResponse('', '');
            clearTimeout(_timer);
        }, 7000);
    }
}

function onSubmitClick(target){
    var hash;
    var _nameField;
    var _emailField;

    if(target){
        if(!target.uniqueId){
            counterId++;
            target.uniqueId = counterId;
        }

        clearTimeout(_doc["timer"+target.uniqueId]);

        _form = target.parentElement;
        _boxSuccessMsg = _form.querySelector("div[id='lista-integracao-sucessmsg']");

        _nameField = _form.querySelector("input[id='lista-integracao-nome']");
        _emailField = _form.querySelector("input[id='lista-integracao-email']");
    }else{
        clearTimeout(_timer);

        _form = document.getElementById('lista-integracao-form');
        _boxSuccessMsg = document.getElementById('lista-integracao-sucessmsg');

        _nameField = document.getElementById('lista-integracao-nome');
        _emailField = document.getElementById('lista-integracao-email');
    }

    _btn = _form.getElementsByTagName('button')[0];
    hash = _form.getAttribute("hash");

    _boxSuccessMsg.innerHTML = 'Enviando...';
    if(_boxSuccessMsg.getAttribute('color')!=null)
        _boxSuccessMsg.setAttribute('color', '');

    _boxSuccessMsg.className = '';

    if(_nameField.value.trim()=='' && _nameField.hasAttribute('required')){
        requestResponse("status-error", "O campo 'Nome' � obrigat�rio.", _boxSuccessMsg, _form);

        clearTimer(target ? target.uniqueId : null, _boxSuccessMsg);
        return;
    }
    else if(_emailField.value.trim()==''){
        requestResponse("status-error", "O campo 'E-mail' � obrigat�rio.", _boxSuccessMsg, _form);
        clearTimer(target ? target.uniqueId : null, _boxSuccessMsg);
        return;
    }

    _btn.disabled=true;

    var _formData = "hash="+hash+"&nome="+_nameField.value+"&email="+_emailField.value;
    postRequest(_formData, target ? target.uniqueId : null, _btn, _boxSuccessMsg);
}
