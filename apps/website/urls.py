from django.conf.urls import patterns, url
from django.views.generic import TemplateView
from apps.multimidia.views import MagazineListView, PublicationListView
from apps.website.views import NewsListView, EventListView, NewsDetailView, ContactView, EventDetailView, \
    FenaconMidiaListView, PressClippingListView, FenaconMidiaDetailView, PressClippingDetailView, ContestSubscriberCreateView
from apps.website.api import PromotionSubscriberListAPI


urlpatterns = patterns('',
    url(r'^noticias/$', NewsListView.as_view(), name='news_list'),
    url(r'^noticias/(?P<slug>[\w_-]+)-(?P<pk>\d+)/$', NewsDetailView.as_view(), name='news_detail'),
    url(r'^noticias/press-clipping/$', PressClippingListView.as_view(), name='pressclipping_list'),
    url(r'^noticias/press-clipping/(?P<slug>[\w_-]+)-(?P<pk>\d+)/$', PressClippingDetailView.as_view(), name='pressclipping_detail'),
    url(r'^noticias/fenacon-na-midia/$', FenaconMidiaListView.as_view(), name='fenaconmidia_list'),
    url(r'^noticias/fenacon-na-midia/(?P<slug>[\w_-]+)-(?P<pk>\d+)/$', FenaconMidiaDetailView.as_view(), name='fenaconmidia_detail'),
    url(r'^eventos/(?P<slug>[\w_-]+)-(?P<pk>\d+)/$', EventDetailView.as_view(), name='event_detail'),
    url(r'^eventos/$', EventListView.as_view(), name='event_list'),
    # url(r'^contato/$', ContactView.as_view(), name='contact'),
    url(r'^concurso/$',  ContestSubscriberCreateView.as_view(), name='concurso'),
    url(r'^api/promocao/participantes/$', PromotionSubscriberListAPI.as_view(),
        name='promotion_subscriber_api'),
    url(r'^location/$',  'apps.website.views.get_location', name='location'),
)
