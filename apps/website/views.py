# -*- coding: utf-8 -*-
from datetime import datetime
from itertools import chain
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.shortcuts import redirect
from django.views.generic import TemplateView, ListView, DetailView, CreateView
from django.http import HttpResponseRedirect, HttpResponse
from django.core.mail import EmailMessage
from apps.multimidia.models import Magazine, Video
from apps.newsletter.forms import SubscriberForm
from apps.newsletter.models import Subscriber
from apps.office.forms import FormState
from apps.website.forms import FormContact, ContestSubscriberForm
from apps.website.models import News, Institute, ContributionGuide, Banner, Event, NewsCategory, FenaconMidia, \
    PressClipping, ContestSubscriber
import requests
from apps.newsletter.utils import get_mailchimp_api
import mailchimp
from urllib2 import urlopen
import json


class HomeView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        try:
            news = News.objects.filter(status=True, main_featured=True).exclude(featured_image='').exclude(category__slug='rede-de-noticias')
            events = Event.objects.filter(status=True, main_featured=True, ends_in__gt=datetime.today()).exclude(featured_image='')
            videos = Video.objects.filter(status=True, main_featured=True)
            main_featured = list(chain(news, events, videos))
            main_featured = list(sorted(main_featured, key=lambda item: item.published_at, reverse=True))
            context['main_featured'] = main_featured
        except:
            pass
        try:
            news = News.objects.filter(status=True, featured=True).exclude(featured_image='').exclude(category__slug='rede-de-noticias')
            events = Event.objects.filter(status=True, featured=True, ends_in__gt=datetime.today()).exclude(featured_image='')
            featured = list(chain(news, events))
            featured = list(sorted(featured, key=lambda item: item.published_at, reverse=True))
            context['featured'] = featured[:2]
        except:
            pass
        try:
            response = requests.get('https://graph.facebook.com/150343591692705/posts',
                                    params={'access_token': '1646697235569741|VdRxG_00-3Q5sCXn26U4lk-Km7k'})
            data = response.json()['data']
            context['post_id'] = data[0]['id'].split('_')[-1]
        except:
            pass
        context['form'] = FormState()
        context['last_news'] = News.objects.filter(status=True, main_featured=False, featured=False).exclude(category__slug='rede-de-noticias').order_by('-published_at')[:3]
        state = self.request.session.get('state', False)
        if state:
            context['state_news'] = News.objects.filter(status=True, main_featured=False, featured=False, state=state).exclude(category__slug='rede-de-noticias').order_by('-published_at')[:3]
        context['institutes'] = Institute.objects.filter(status=True)[:2]
        context['guide'] = ContributionGuide.objects.filter(status=True)[:1]
        context['magazine'] = Magazine.objects.filter(status=True, type='0').order_by('-created_at')[:1]
        context['banners'] = Banner.objects.filter(status=True, start_at__lte=datetime.today, ends_in__gte=datetime.today).order_by('-created_at')

        return context

    def post(self, request):
        form = SubscriberForm(request.POST)
        if form.is_valid():
            subscriber = Subscriber()
            subscriber.name = form.cleaned_data['name']
            subscriber.email = form.cleaned_data['email']
            subscriber.save()
            try:
                m = get_mailchimp_api()
                m.lists.subscribe('86063c8fb2', {'email': subscriber.email})
            except:
                pass
            messages.success(request, 'Agora você receberá nossos informativos')
        else:
            messages.error(request, 'Digite seu nome e um email válido.')

        return redirect(reverse('home') + '#subscriptions')


def get_location(request):
    flag = request.session.get('state', False)
    if not flag:
        lat = request.GET.get('latitude', None)
        lon = request.GET.get('longitude', None)
        url = "http://maps.googleapis.com/maps/api/geocode/json?"
        url += "latlng=%s,%s&sensor=false" % (lat, lon)
        v = urlopen(url).read()
        j = json.loads(v)
        components = j['results'][0]['address_components']
        state = None
        for c in components:
            if "administrative_area_level_1" in c['types']:
                state = c['short_name']
        request.session['state'] = state
        return HttpResponse('reload')
    else:
        return HttpResponse('ok')


class SearchView(TemplateView):
    template_name = 'listagem-busca.html'

    def get(self, request, *args, **kwargs):
        q = self.request.GET.get('q')
        context = self.get_context_data(**kwargs)
        if q:
            context['news'] = News.objects.filter(Q(title__icontains=q, status=True) | Q(subtitle__icontains=q, status=True))
            context['videos'] = Video.objects.filter(title__icontains=q, status=True)
            context['publications'] = Magazine.objects.filter(status=True, number__icontains=q)
        return self.render_to_response(context)


class ContactView(TemplateView):
    template_name = 'contato.html'

    def post(self, request, *args, **kwargs):
        form = FormContact(request.POST)
        if form.is_valid():
            form.send()
            messages.success(self.request, 'Mensagem enviada com sucesso')
        if form.errors:
            messages.warning(self.request, 'Preencha todos os campos obrigatórios que possuam "*"')
        return redirect('contact')

    def get_context_data(self, **kwargs):
        context = super(ContactView, self).get_context_data(**kwargs)
        context['form'] = FormContact()
        return context


class NewsListView(ListView):
    model = News
    paginate_by = 8
    template_name = 'website/listagem-noticia.html'

    def get_context_data(self, **kwargs):
        context = super(NewsListView, self).get_context_data(**kwargs)
        context['categories'] = NewsCategory.objects.filter(status=True)
        return context

    def get_queryset(self):
        category = self.request.GET.get('category')
        initial_date = self.request.GET.get('initial-date')
        final_date = self.request.GET.get('final-date')
        object_list = News.objects.filter(status=True).order_by('-published_at')
        if category:
            object_list = object_list.filter(category__slug=category)
        if initial_date:
            initial_date = datetime.strptime(initial_date, '%d/%m/%Y')
            object_list = object_list.filter(published_at__gte=initial_date)
        if final_date:
            final_date = datetime.strptime(final_date, '%d/%m/%Y')
            object_list = object_list.filter(published_at__lte=final_date)

        return object_list


class FenaconMidiaListView(ListView):
    model = FenaconMidia
    paginate_by = 8
    template_name = 'website/listagem-fenaconmidia.html'

    def get_queryset(self):
        initial_date = self.request.GET.get('initial-date')
        final_date = self.request.GET.get('final-date')
        object_list = FenaconMidia.objects.filter(status=True)
        news_list = News.objects.filter(fenacon_midia=True, status=True)
        if initial_date:
            object_list = object_list.filter(published_at__gte=initial_date)
            news_list = news_list.filter(published_at__gte=initial_date)
        if final_date:
            object_list = object_list.filter(published_at__lte=final_date)
            news_list = news_list.filter(published_at__lte=final_date)
        result_list = sorted(chain(object_list, news_list), key=lambda item: item.published_at, reverse=True)
        return result_list


class PressClippingListView(ListView):
    model = PressClipping
    paginate_by = 8
    template_name = 'website/listagem-pressclipping.html'

    def get_queryset(self):
        initial_date = self.request.GET.get('initial-date')
        final_date = self.request.GET.get('final-date')
        object_list = PressClipping.objects.all().order_by('-published_at')
        if initial_date:
            object_list = object_list.filter(published_at__gte=initial_date)
        if final_date:
            object_list = object_list.filter(published_at__lte=final_date)

        return object_list


class NewsDetailView(DetailView):
    model = News
    template_name = 'website/noticia.html'

    def get_context_data(self, **kwargs):
        context = super(NewsDetailView, self).get_context_data(**kwargs)
        context['related_news'] = News.objects.filter(status=True, category=self.object.category).exclude(pk=self.object.pk).order_by('-published_at')[:2]
        return context


class FenaconMidiaDetailView(DetailView):
    model = FenaconMidia
    template_name = 'website/noticia.html'

    def get_context_data(self, **kwargs):
        context = super(FenaconMidiaDetailView, self).get_context_data(**kwargs)
        context['related_news'] = FenaconMidia.objects.filter(status=True).exclude(pk=self.object.pk).order_by('-published_at')[:2]
        return context


class PressClippingDetailView(DetailView):
    model = PressClipping
    template_name = 'website/press_clipping.html'


class EventListView(ListView):
    model = Event
    paginate_by = 8
    template_name = 'website/listagem-eventos.html'

    def get_queryset(self):
        region = self.request.GET.get('region')
        object_list = Event.objects.filter(status=True, ends_in__gt=datetime.today()).order_by('start_at')
        if region:
            object_list = object_list.filter(region=region, ends_in__gt=datetime.today()).order_by('start_at')

        return object_list


class EventDetailView(DetailView):
    model = Event
    template_name = 'website/evento.html'


class ContestSubscriberCreateView(CreateView):
    model = ContestSubscriber
    form_class = ContestSubscriberForm
    template_name = 'website/concurso.html'

    def get_success_url(self):
        messages.success(self.request, 'Inscrição enviada com sucesso!')
        return reverse('concurso')

    def form_valid(self, form):
        self.object = form.save()
        title = 'Inscrição do site Fenacon - Prêmio Jubileu de Prata'
        text = """
        Nome: %(name)s
        E-mail: %(email)s
        Telefone: %(phone)s
        CPF: %(cpf)s
        CEP: %(cep)s
        Endereco: %(address)s
        """ % form.cleaned_data
        file = self.request.FILES['file']
        work_file = self.request.FILES['work_file']
        mail = EmailMessage(title, text, 'notificacaofenacon@gmail.com', ['comunica@fenacon.org.br', 'jornalista@fenacon.org.br'])
        mail.attach(file.name, file.read(), file.content_type)
        mail.attach(work_file.name, work_file.read(), work_file.content_type)
        mail.send()

        return HttpResponseRedirect(self.get_success_url())
