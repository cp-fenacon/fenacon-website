from apps.accounts.models import User
from rest_framework import generics, filters, parsers, renderers, mixins
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.authtoken.models import Token
from rest_framework.views import APIView
import django_filters
from django_filters import rest_framework
from apps.website.models import (PromotionSubscriber, News, NewsCategory,
                                 PressClipping, FenaconMidia, Event)
from apps.office.models import Office, Syndicate
from apps.flatpages.models import AboutUs
from apps.multimidia.models import Magazine
from apps.website.serializers import (PromotionSubscriberSerializer,
                                      NewsSerializer, UserSerializer,
                                      FullUserSerializer, CreateUserSerializer,
                                      NewsCategorySerializer, EventSerializer,
                                      PressClippingSerializer,
                                      FenaconMidiaSerializer, OfficeSerializer,
                                      SyndicateSerializer, AboutUsSerializer,
                                      CustomAuthTokenSerializer,
                                      MagazineSerializer)


class PromotionSubscriberListAPI(generics.CreateAPIView):
    queryset = PromotionSubscriber.objects.all()
    serializer_class = PromotionSubscriberSerializer


class UserListAPI(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('username', 'first_name', 'last_name')


class NewsCategoryListAPI(generics.ListAPIView):
    queryset = NewsCategory.objects.all()
    serializer_class = NewsCategorySerializer
    filter_backends = (
        filters.DjangoFilterBackend,
        filters.SearchFilter)
    filter_fields = ('status',)
    search_fields = ('title',)


class NewsListAPI(generics.ListAPIView):
    queryset = News.objects.filter(status=True)
    serializer_class = NewsSerializer
    filter_backends = (
        filters.DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter)
    filter_fields = ('category', 'fenacon_midia', 'state', 'is_mobile')
    search_fields = ('title', 'text', 'author')
    ordering_fields = ('published_at',)


class NewsDetailAPI(generics.GenericAPIView, mixins.RetrieveModelMixin):
    serializer_class = NewsSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def get_object(self):
        obj = News.objects.get(pk=self.kwargs['pk'])
        return obj


class EventFilter(rest_framework.FilterSet):
    published_at = django_filters.DateTimeFilter(
        name="published_at", lookup_expr="gte")
    start_at = django_filters.DateTimeFilter(
        name="start_at", lookup_expr="gte")
    ends_in = django_filters.DateTimeFilter(name="ends_in", lookup_expr="gte")

    class Meta:
        model = Event
        fields = [
            'status', 'main_featured', 'featured', 'region',
            'published_at', 'start_at', 'ends_in']


class EventListAPI(generics.ListAPIView):
    queryset = Event.objects.filter(status=True)
    serializer_class = EventSerializer
    filter_backends = (
        filters.DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter)
    filter_class = EventFilter
    search_fields = ('title', 'text', 'city', 'local', 'region', 'credit')
    ordering_fields = ('published_at', 'start_at', 'ends_in')


class FenaconMidiaListAPI(generics.ListAPIView):
    queryset = FenaconMidia.objects.filter(status=True)
    serializer_class = FenaconMidiaSerializer
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter)
    search_fields = ('title', 'subtitle', 'text')
    ordering_fields = ('published_at',)


class PressClippingListAPI(generics.ListAPIView):
    queryset = PressClipping.objects.all()
    serializer_class = PressClippingSerializer
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter)
    search_fields = ('title', 'text')
    ordering_fields = ('published_at',)


class PressClippingAPI(generics.GenericAPIView, mixins.RetrieveModelMixin):
    serializer_class = PressClippingSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def get_object(self):
        obj = PressClipping.objects.get(pk=self.kwargs['pk'])
        return obj


class SyndicateListAPI(generics.ListAPIView):
    queryset = Syndicate.objects.all()
    serializer_class = SyndicateSerializer
    filter_backends = (
        filters.DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter)
    filter_fields = ('region', 'state')
    search_fields = ('name', 'president', 'email')
    ordering_fields = ('created_at',)


class OfficeListAPI(generics.ListAPIView):
    queryset = Office.objects.all()
    serializer_class = OfficeSerializer
    filter_backends = (
        filters.DjangoFilterBackend,
        filters.SearchFilter)
    filter_fields = ('state',)
    search_fields = ('cnpj', 'company_name', 'city', 'cep', 'emails', 'phone')


class AboutUsListAPI(generics.ListAPIView):
    queryset = AboutUs.objects.all()
    serializer_class = AboutUsSerializer


class MagazineListAPI(generics.ListAPIView):
    queryset = Magazine.objects.all()
    serializer_class = MagazineSerializer
    filter_backends = (
        filters.DjangoFilterBackend,
        filters.SearchFilter)
    filter_fields = ('type',)
    search_fields = ('number',)


class CreateUserAPI(generics.CreateAPIView):
    serializer_class = CreateUserSerializer


class ObtainAuthToken(APIView):
    throttle_classes = ()
    permission_classes = ()
    parser_classes = (
        parsers.FormParser, parsers.MultiPartParser, parsers.JSONParser)
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = CustomAuthTokenSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        user_serializer = FullUserSerializer(user)
        token, created = Token.objects.get_or_create(user=user)
        return Response({'token': token.key, 'user': user_serializer.data})


@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'news': reverse('news_list_api',
                        request=request, format=format),
        'events': reverse('event_list_api',
                          request=request, format=format),
        'press-clipping': reverse('pressclipping_list_api',
                                  request=request, format=format),
        'fenacon-midia': reverse('fenaconmidia_list_api',
                                 request=request, format=format),
        'category': reverse('category_list_api',
                            request=request, format=format),
        'syndicate': reverse('syndicate_list_api',
                             request=request, format=format),
        'office': reverse('office_list_api',
                          request=request, format=format),
        'about-us': reverse('aboutus_list_api',
                            request=request, format=format),
        'magazine': reverse('magazine_list_api',
                            request=request, format=format),
    })
