# coding=utf-8
from django.contrib import admin
from image_cropping import ImageCroppingMixin
from apps.website.models import SocialNetwork, Banner, NewsCategory, News, Event, ContributionGuide, Institute, \
    FenaconMidia, PressClipping, ContestSubscriber, PromotionSubscriber


class InstituteAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_filter = ['status']
    list_display = ('name', 'show_image', 'user', 'status')
    fieldsets = [
        (None, {'fields': ['name', 'description', 'link']}),
        ('Imagens', {'fields': ['featured_image']}),
        (u'Informações', {'fields': ['status']}),
    ]
    def show_image(self, obj):
        if obj.featured_image:
            return u'<img src="%s" width="50px" heigth="50px"/>' % obj.featured_image.url
        else:
            return u''
    show_image.allow_tags = True
    show_image.short_description = u'Imagem'

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()


class BannerAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_filter = ['status']
    list_display = ('show_image', 'start_at', 'ends_in', 'created_at', 'user', 'status')
    fieldsets = [
        (None, {'fields': ['link']}),
        ('Imagens', {'fields': ['photo']}),
        (u'Informações', {'fields': ['start_at', 'ends_in', 'status']}),
    ]
    def show_image(self, obj):
        if obj.photo:
            return u'<img src="%s" width="50px" heigth="50px"/>' % obj.photo.url
        else:
            return u''
    show_image.allow_tags = True
    show_image.short_description = u'Imagem'

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()


class NewsAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ('title', 'show_image', 'published_at', 'user', 'see_post')
    search_fields = ('title', 'text')
    list_filter = ('status', 'category', 'fenacon_midia', 'state', 'is_mobile')

    def see_post(self, obj):
        return u'<a class="button" target="_blank" href="%s">Ver no site</a>' % obj.get_absolute_url()
    see_post.allow_tags = True
    see_post.short_description = u'Ver no site'

    def show_image(self, obj):
        if obj.featured_image:
            return u'<img src="%s" width="50px" heigth="50px"/>' % obj.featured_image.url
        else:
            return u''
    show_image.allow_tags = True
    show_image.short_description = u'Imagem'
    fieldsets = [
        (None, {'fields': ['title', 'subtitle', 'text', 'font', 'author', 'category', 'state', 'published_at']}),
        ('Imagens', {'fields': ['featured_image', 'featured_big', 'featured_medium',
                                'featured_small', 'credit']}),
        (u'Informações', {'fields': ['status', 'is_mobile', 'main_featured', 'featured', 'fenacon_midia']}),
    ]

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()


class FenaconMidiaAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ('title', 'show_image', 'published_at', 'user', 'see_post')
    search_fields = ('title', 'text')
    list_filter = ('status',)

    def see_post(self, obj):
        return u'<a class="button" target="_blank" href="%s">Ver no site</a>' % obj.get_absolute_url()
    see_post.allow_tags = True
    see_post.short_description = u'Ver no site'

    def show_image(self, obj):
        if obj.featured_image:
            return u'<img src="%s" width="50px" heigth="50px"/>' % obj.featured_image.url
        else:
            return u''
    show_image.allow_tags = True
    show_image.short_description = u'Imagem'
    fieldsets = [
        (None, {'fields': ['title', 'subtitle', 'text', 'font', 'author', 'published_at']}),
        ('Imagens', {'fields': ['featured_image', 'featured_big', 'featured_medium',
                                'featured_small', 'credit']}),
        (u'Informações', {'fields': ['status']}),
    ]

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()


class EventAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ('title', 'user', 'published_at', 'status')
    list_filter = ['status', 'region']
    search_fields = ['title']
    fieldsets = [
        (None, {'fields': ['title', 'text']}),
        (u'Imagens', {'fields': ['credit', 'featured_image', 'featured_big', 'featured_medium', 'featured_small']}),
        (u'Informações', {'fields': ['region', 'city', 'local', 'start_at', 'ends_in', 'status', 'main_featured', 'featured']}),
    ]

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()



class NewsCategoryAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ('title', 'status')
    list_filter = ['status']
    search_fields = ['title']
    fieldsets = [
        (None, {'fields': ['title']}),
        (u'Informações', {'fields': ['status']}),
    ]


class PressClippingAdmin(admin.ModelAdmin):
    list_display = ('title', 'published_at')
    search_fields = ['title', 'text']
    fieldsets = [
        (None, {'fields': ['title', 'text', 'published_at']}),
    ]


class ContestSubscriberAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'cpf', 'phone')
    search_fields = ['name', 'email', 'address', 'cpf', 'cep', 'phone']


class PromotionSubscriberAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'email', 'phone', 'registration_number')
    search_fields = ['name', 'email', 'registration_number', 'phone']


admin.site.register(Institute, InstituteAdmin)
admin.site.register(ContributionGuide)
admin.site.register(SocialNetwork)
admin.site.register(Banner, BannerAdmin)
admin.site.register(NewsCategory, NewsCategoryAdmin)
admin.site.register(News, NewsAdmin)
admin.site.register(FenaconMidia, FenaconMidiaAdmin)
admin.site.register(PressClipping, PressClippingAdmin)
admin.site.register(Event, EventAdmin)
admin.site.register(ContestSubscriber, ContestSubscriberAdmin)
admin.site.register(PromotionSubscriber, PromotionSubscriberAdmin)
