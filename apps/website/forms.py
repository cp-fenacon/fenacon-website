# -*- coding: utf-8 -*-
from django import forms
from django.core.mail import send_mail
from apps.website.models import ContestSubscriber


SUBJECT = {('Dúvida', 'Dúvida'),
          ('Informação', 'Informação'),
          ('Sugestão', 'Sugestão'),
          ('Reclamação', 'Reclamação'),
          ('Lei Geral', 'Lei Geral'),
          ('Empreendedor Individual', 'Empreendedor Individual'),
          ('Outros', 'Outros')}


class FormContact(forms.Form):
    name = forms.CharField(max_length=50, required=True)
    email = forms.EmailField(required=True)
    phone = forms.CharField(max_length=50, required=True)
    subject = forms.ChoiceField(choices=SUBJECT, required=True)
    cnpj = forms.CharField(max_length=50, required=False)
    message = forms.Field(widget=forms.Textarea, required=True)

    def send(self):
        title = 'Mensagem do site Fenacon - Contato'
        target = 'fenacon@fenacon.org.br'
        text = """
        Nome: %(name)s
        E-mail: %(email)s
        Telefone: %(phone)s
        Assunto: %(subject)s
        CNPJ: %(cnpj)s
        Mensagem: %(message)s
        """ % self.cleaned_data

        send_mail(
            subject=title,
            message=text,
            from_email=target,
            recipient_list=[target],
            )


class ContestSubscriberForm(forms.ModelForm):
    class Meta:
        model = ContestSubscriber
        fields = ['name', 'email', 'address', 'cpf', 'cep', 'phone', 'file', 'work_file']
