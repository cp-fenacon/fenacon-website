# coding=utf-8
from datetime import datetime
from apps.accounts.models import User
from django.db import models
from django.db.models import signals
from django.template.defaultfilters import slugify
from easy_thumbnails.files import get_thumbnailer
from image_cropping import ImageRatioField
from ckeditor_uploader.fields import RichTextUploadingField
import requests

REGION = {('norte', 'Norte'),
          ('nordeste', 'Nordeste'),
          ('centro-oeste', 'Centro-Oeste'),
          ('sudeste', 'Sudeste'),
          ('sul', 'Sul')}

UF_CHOICES = (
    ('', 'Nacional'),
    ('AC', 'Acre'),
    ('AL', 'Alagoas'),
    ('AP', 'Amapá'),
    ('AM', 'Amazonas'),
    ('BA', 'Bahia'),
    ('CE', 'Ceará'),
    ('DF', 'Distrito Federal'),
    ('ES', 'Espírito Santo'),
    ('GO', 'Goiás'),
    ('MA', 'Maranhão'),
    ('MT', 'Mato Grosso'),
    ('MS', 'Mato Grosso do Sul'),
    ('MG', 'Minas Gerais'),
    ('PA', 'Pará'),
    ('PB', 'Paraíba'),
    ('PR', 'Paraná'),
    ('PE', 'Pernambuco'),
    ('PI', 'Piauí'),
    ('RJ', 'Rio de Janeiro'),
    ('RN', 'Rio Grande do Norte'),
    ('RS', 'Rio Grande do Sul'),
    ('RO', 'Rondônia'),
    ('RR', 'Roraima'),
    ('SC', 'Santa Catarina'),
    ('SP', 'São Paulo'),
    ('SE', 'Sergipe'),
    ('TO', 'Tocantins')
)


class SocialNetwork(models.Model):
    facebook = models.URLField(null=True, blank=True)
    instagram = models.URLField(null=True, blank=True)
    twitter = models.URLField(null=True, blank=True)
    youtube = models.URLField(null=True, blank=True)
    telefax = models.CharField(null=True, blank=True, max_length=30)
    address = models.CharField('Endereço FENACON', null=True, blank=True, max_length=200)
    cep = models.CharField('CEP da FENACON', null=True, blank=True, max_length=200)
    address_imprensa = models.CharField('Endereço Assessoria', null=True, blank=True, max_length=200)
    cep_imprensa = models.CharField('CEP Assessoria', null=True, blank=True, max_length=200)
    phone = models.CharField('Telefones Assessoria', null=True, blank=True, max_length=30)
    email = models.EmailField("Email da asessoria de impresa", null=True, blank=True)

    def __unicode__(self):
        return 'Contato FENACON'

    class Meta:
        verbose_name = "Contato"
        verbose_name_plural = "Contato"


class Banner(models.Model):
    photo = models.ImageField('Foto', upload_to='uploads/banner/', blank=True, null=True, help_text="Dimensões 870x135px")
    link = models.URLField(help_text="Endereço da página", blank=True, null=True)
    created_at = models.DateTimeField(verbose_name='Data do Cadastro', blank=True, default=datetime.now)
    start_at = models.DateTimeField(verbose_name='Começa em', default=datetime.now)
    ends_in = models.DateTimeField(verbose_name='Términa em')
    user = models.ForeignKey(User, verbose_name='Usuário')
    status = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Banner'
        verbose_name_plural = 'Banners'

    def __unicode__(self):
        return u'%s' % str(self.photo).split('/')[-1]


class Institute(models.Model):
    name = models.CharField("Nome", max_length=90)
    description = models.CharField("Descrição", max_length=140)
    link = models.URLField(help_text="Endereço da página")
    featured_image = models.ImageField("Imagem", upload_to='uploads/instituto/%Y/%m/', help_text="Dimensões 275x70px")
    status = models.BooleanField(verbose_name='Ativo?', default=True, help_text="Se esta opção estiver desmarcada os usuários do portal não irão mais ver este instituição.")
    user = models.ForeignKey(User, verbose_name='Usuário')

    class Meta:
        verbose_name = u'Instituto Fenacon'
        verbose_name_plural = u'Institutos Fenacon'

    def __unicode__(self):
        return self.name


class ContributionGuide(models.Model):
    link = models.URLField(help_text="Endereço da página")
    status = models.BooleanField(verbose_name='Ativo?', default=True, help_text="Se esta opção estiver desmarcada os usuários do portal não irão mais ver este instituição.")

    class Meta:
        verbose_name = u'Guia de contribuição sindical'
        verbose_name_plural = u'Guia de contribuição sindical'

    def __unicode__(self):
        return self.link


class Event(models.Model):
    title = models.CharField("Título", max_length=90, help_text="Titulo do Evento")
    text = RichTextUploadingField(verbose_name="Texto", null=True, blank=True)
    slug = models.SlugField(max_length=200, blank=True)
    city = models.CharField('Cidade/Estado', max_length=50, null=True, blank=True)
    local = models.CharField('Endereço', max_length=150)
    region = models.CharField('Região', max_length=50, choices=REGION, null=True, blank=True)
    credit = models.CharField("Crédito", max_length=30, blank=True, help_text="Creditos da foto.")
    featured_image = models.ImageField("Imagem Destaque", null=True, blank=True, upload_to='uploads/eventos/%Y/%m/', help_text="Dimensões 600x335 ou maior - JPEG")
    featured_big = ImageRatioField('featured_image', '600x335', verbose_name='Destaque Grande', help_text="Destaque princial na home.")
    featured_medium = ImageRatioField('featured_image', '470x180', verbose_name='Destaque Médio', help_text="Destaque secundário home.")
    featured_small = ImageRatioField('featured_image', '170x170', verbose_name='Destaque Pequeno', help_text="Imagem para listagem de eventos.")
    published_at = models.DateTimeField(verbose_name='Data de Publicacao', default=datetime.now)
    start_at = models.DateTimeField(verbose_name='Começa em', default=datetime.now)
    ends_in = models.DateTimeField(verbose_name='Términa em')
    status = models.BooleanField(verbose_name='Evento Ativo?', default=True, help_text="Se esta opção estiver desmarcada os usuários do portal não irão mais ver este evento.")
    main_featured = models.BooleanField(verbose_name='Destaque principal', default=False, help_text="Coloca a notícia em destaque. Deve possuir imagem destaque.")
    featured = models.BooleanField(verbose_name='Destaque secundário', default=False, help_text="Coloca a notícia em destaque. Deve possuir imagem destaque.")
    user = models.ForeignKey(User, verbose_name='Usuário')


    class Meta:
        ordering = ('-published_at',)
        verbose_name = u'Evento'
        verbose_name_plural = u'Eventos'
        get_latest_by = 'published_at'

    @models.permalink
    def get_absolute_url(self):
        return ('event_detail', [self.slug, self.pk])

    def __unicode__(self):
        return self.title

    def get_featured_big(self):
        return get_thumbnailer(self.featured_image).get_thumbnail({'size': (600, 335), 'box': self.featured_big, 'crop': True, 'detail': True, }).url

    def get_featured_medium(self):
        return get_thumbnailer(self.featured_image).get_thumbnail({'size': (470, 180), 'box': self.featured_medium, 'crop': True, 'detail': True, }).url

    def get_featured_small(self):
        return get_thumbnailer(self.featured_image).get_thumbnail({'size': (170, 170), 'box': self.featured_small, 'crop': True, 'detail': True, }).url


class NewsCategory(models.Model):
    title = models.CharField("Título", max_length=90, help_text="Titulo da Categoria")
    slug = models.SlugField(max_length=200, blank=True, unique=True)
    status = models.BooleanField(default=True)

    class Meta:
        verbose_name = u'Categoria de Notícia'
        verbose_name_plural = u'Categorias de Notícias'

    def __unicode__(self):
        return self.title


class ContestSubscriber(models.Model):
    # def update_filename(instance, filename):
    #     path = "uploads/concurso/"
    #     fname = filename.split('.')
    #     format = slugify(filename) + '.' + fname[-1]
    #     return os.path.join(path, format)
    name = models.CharField("Nome", max_length=140)
    cpf = models.CharField("CPF", max_length=15)
    address = models.CharField("Endereço", max_length=200)
    cep = models.CharField("CEP", max_length=200)
    phone = models.CharField("Telefone", max_length=200)
    email = models.EmailField()
    file = models.FileField("Arquivo declaração", upload_to='uploads/concurso/')
    work_file = models.FileField("Arquivo de trabalho", upload_to='uploads/concurso/')

    class Meta:
        verbose_name = u'Inscrição de concurso'
        verbose_name_plural = u'Inscrições de concurso'

    def __unicode__(self):
        return self.name


class News(models.Model):
    title = models.CharField("Título", max_length=90, help_text="Titulo da Noticia")
    subtitle = models.CharField("Sub-título", max_length=100, help_text="Para Manchetes", blank=True, null=True)
    text = RichTextUploadingField(verbose_name="Texto")
    slug = models.SlugField(max_length=200, blank=True)
    credit = models.CharField("Crédito", max_length=50, blank=True, help_text="Creditos da foto.")
    font = models.CharField("Fonte", max_length=50, blank=True, help_text="Fonte da notícia")
    author = models.CharField("Autor", max_length=50, blank=True, help_text="Autor da notícia")
    featured_image = models.ImageField("Imagem Destaque", upload_to='uploads/noticias/%Y/%m/', blank=True, help_text="Dimensões 600x335px ou maior - JPEG")
    featured_big = ImageRatioField('featured_image', '600x335', verbose_name='Destaque Grande', help_text="Destaque princial na home.")
    featured_medium = ImageRatioField('featured_image', '470x180', verbose_name='Destaque Médio', help_text="Destaque secundário home.")
    featured_small = ImageRatioField('featured_image', '170x170', verbose_name='Destaque Pequeno', help_text="Imagem para listagem de notícias e últimas noíticas.")
    published_at = models.DateTimeField(verbose_name='Data de Publicacao', default=datetime.now)
    user = models.ForeignKey(User, verbose_name='Usuário')
    status = models.BooleanField(verbose_name='Noticia Ativa?', default=True, help_text="Se esta opção estiver desmarcada os usuários do portal não irão mais ver esta notícia")
    main_featured = models.BooleanField(verbose_name='Destaque principal', default=False, help_text="Coloca a notícia em destaque. Deve possuir imagem destaque.")
    featured = models.BooleanField(verbose_name='Destaque secundário', default=False, help_text="Coloca a notícia em destaque. Deve possuir imagem destaque.")
    category = models.ForeignKey(NewsCategory, verbose_name='Categoria', help_text="Categoria que a notícia pertence.")
    fenacon_midia = models.BooleanField(verbose_name='FENACON na mídia', default=False, help_text="Coloca a notícia na listagem de FENACON na mídia.")
    state = models.CharField('Estado', max_length=50, choices=UF_CHOICES, null=True, blank=True)
    is_mobile = models.BooleanField(verbose_name='Habilitar no app?', default=True, help_text="Se esta opção estiver desmarcada os usuários do app não irão mais ver esta notícia")

    class Meta:
        ordering = ('-published_at',)
        verbose_name = u'Notícia'
        verbose_name_plural = u'Notícias'
        get_latest_by = 'published_at'

    @models.permalink
    def get_absolute_url(self):
        return ('news_detail', [self.slug, self.pk])

    def __unicode__(self):
        return self.title

    def get_featured_big(self):
        return get_thumbnailer(self.featured_image).get_thumbnail({'size': (600, 335), 'box': self.featured_big, 'crop': True, 'detail': True, }).url

    def get_featured_medium(self):
        return get_thumbnailer(self.featured_image).get_thumbnail({'size': (470, 180), 'box': self.featured_medium, 'crop': True, 'detail': True, }).url

    def get_featured_small(self):
        return get_thumbnailer(self.featured_image).get_thumbnail({'size': (170, 170), 'box': self.featured_small, 'crop': True, 'detail': True, }).url


class FenaconMidia(models.Model):
    title = models.CharField("Título", max_length=90, help_text="Titulo da Noticia")
    subtitle = models.CharField("Sub-título", max_length=100, help_text="Para Manchetes", blank=True, null=True)
    text = RichTextUploadingField(verbose_name="Texto")
    slug = models.SlugField(max_length=200, blank=True)
    credit = models.CharField("Crédito", max_length=50, blank=True, help_text="Creditos da foto.")
    font = models.CharField("Fonte", max_length=50, blank=True, help_text="Fonte da notícia")
    author = models.CharField("Autor", max_length=50, blank=True, help_text="Autor da notícia")
    featured_image = models.ImageField("Imagem Destaque", upload_to='uploads/noticias/%Y/%m/', blank=True, help_text="Dimensões 600x335px ou maior - JPEG")
    featured_big = ImageRatioField('featured_image', '600x335', verbose_name='Destaque Grande', help_text="Destaque princial na home.")
    featured_medium = ImageRatioField('featured_image', '470x180', verbose_name='Destaque Médio', help_text="Destaque secundário home.")
    featured_small = ImageRatioField('featured_image', '170x170', verbose_name='Destaque Pequeno', help_text="Imagem para listagem de notícias e últimas noíticas.")
    published_at = models.DateTimeField(verbose_name='Data de Publicacao', default=datetime.now)
    user = models.ForeignKey(User, verbose_name='Usuário')
    status = models.BooleanField(verbose_name='Noticia Ativa?', default=True, help_text="Se esta opção estiver desmarcada os usuários do portal não irão mais ver esta notícia")

    class Meta:
        ordering = ('-published_at',)
        verbose_name = u'FENACON na Mídia'
        verbose_name_plural = u'FENACON na Mídia'
        get_latest_by = 'published_at'

    @models.permalink
    def get_absolute_url(self):
        return ('fenaconmidia_detail', [self.slug, self.pk])

    def __unicode__(self):
        return self.title

    def get_featured_big(self):
        return get_thumbnailer(self.featured_image).get_thumbnail({'size': (600, 335), 'box': self.featured_big, 'crop': True, 'detail': True, }).url

    def get_featured_medium(self):
        return get_thumbnailer(self.featured_image).get_thumbnail({'size': (470, 180), 'box': self.featured_medium, 'crop': True, 'detail': True, }).url

    def get_featured_small(self):
        return get_thumbnailer(self.featured_image).get_thumbnail({'size': (170, 170), 'box': self.featured_small, 'crop': True, 'detail': True, }).url


class PressClipping(models.Model):
    title = models.CharField("Título", max_length=90)
    text = RichTextUploadingField(verbose_name="Texto")
    slug = models.SlugField(max_length=200, blank=True)
    published_at = models.DateTimeField(verbose_name='Data de Publicacao', default=datetime.now)

    class Meta:
        ordering = ('-published_at',)
        verbose_name = u'Press Clipping'
        verbose_name_plural = u'Press Clipping'

    @models.permalink
    def get_absolute_url(self):
        return ('pressclipping_detail', [self.slug, self.pk])

    def __unicode__(self):
        return self.title


def fenacon_midia_pre_save(signal, instance, sender, **kwargs):
    if not instance.slug:
        instance.slug = slugify(instance.title)


def press_clipping_pre_save(signal, instance, sender, **kwargs):
    if not instance.slug:
        instance.slug = slugify(instance.title)


def press_clipping_post_save(sender, instance, created, **kwargs):
    if created:
        url = 'http://app.fenacon.org.br/webservices/notificacaoservice.asmx/NotificarPressClipping'
        data = {'pressClipping': {'Id': instance.id, 'Titulo': instance.title}}
        requests.post(url, json=data)


def news_pre_save(signal, instance, sender, **kwargs):
    if not instance.slug:
        instance.slug = slugify(instance.title)


def news_post_save(sender, instance, created, **kwargs):
    if created:
        if instance.is_mobile:
            if instance.featured_image:
                image = instance.featured_image.url
            else:
                image = ''
            url = 'http://app.fenacon.org.br/webservices/notificacaoservice.asmx/NotificarNoticia'
            data = {'noticia': {
                'Titulo': instance.title,
                'UrlImagem': image, 'Id': instance.id}}
            requests.post(url, json=data)


def event_pre_save(signal, instance, sender, **kwargs):
    if not instance.slug:
        instance.slug = slugify(instance.title)


def newscategory_pre_save(signal, instance, sender, **kwargs):
    if not instance.slug:
        instance.slug = slugify(instance.title)


signals.pre_save.connect(fenacon_midia_pre_save, sender=FenaconMidia)
signals.pre_save.connect(press_clipping_pre_save, sender=PressClipping)
signals.post_save.connect(press_clipping_post_save, sender=PressClipping)
signals.pre_save.connect(newscategory_pre_save, sender=NewsCategory)
signals.pre_save.connect(news_pre_save, sender=News)
signals.post_save.connect(news_post_save, sender=News)
signals.pre_save.connect(event_pre_save, sender=Event)


class PromotionSubscriber(models.Model):
    name = models.CharField("nome", max_length=90)
    phone = models.CharField("telefone", max_length=90)
    email = models.EmailField()
    registration_number = models.CharField("numero de inscricao", max_length=200)

    class Meta:
        verbose_name = u'Participante da promoção'
        verbose_name_plural = u'Participantes da promoção'

    def __unicode__(self):
        return self.name
