# coding=utf-8
from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from apps.accounts.models import User
from apps.website.models import (PromotionSubscriber, News, NewsCategory,
                                 PressClipping, FenaconMidia, Event)
from apps.office.models import Syndicate, Office
from apps.flatpages.models import AboutUs
from apps.multimidia.models import Magazine


class PromotionSubscriberSerializer(serializers.ModelSerializer):
    class Meta:
        model = PromotionSubscriber
        fields = ('name', 'phone', 'email', 'registration_number')


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name')


class FullUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id', 'username', 'first_name', 'last_name', 'email', 'last_login')


class CreateUserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=False)

    class Meta:
        model = User
        fields = (
            'id', 'email', 'username', 'first_name', 'last_name',
            'google_token', 'facebook_token', 'password')
        write_only_fields = ('password',)
        read_only_fields = ('id',)

    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username'],
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            google_token=validated_data.get('google_token'),
            facebook_token=validated_data.get('facebook_token'),
        )

        if validated_data.get('google_token') or validated_data.get('facebook_token') or validated_data.get('password'):
            if validated_data.get('password'):
                user.set_password(validated_data.get('password'))
            user.save()
        else:
            raise serializers.ValidationError({
                'non_field_errors': 'Login social ou password são obrigatórios.'
            })

        return user


class NewsCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = NewsCategory
        fields = ('id', 'title', 'slug', 'status')


class NewsSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    category = NewsCategorySerializer()

    class Meta:
        model = News
        fields = (
            'id', 'title', 'subtitle', 'text', 'font', 'author', 'category',
            'state', 'published_at', 'featured_image', 'credit', 'user',
            'main_featured', 'featured', 'fenacon_midia', 'is_mobile')


class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = (
            'id', 'title', 'text', 'slug', 'city', 'local', 'region',
            'credit', 'featured_image', 'featured_big', 'featured_medium',
            'featured_small', 'published_at', 'start_at', 'ends_in',
            'status', 'main_featured', 'featured')


class PressClippingSerializer(serializers.ModelSerializer):
    class Meta:
        model = PressClipping
        fields = ('id', 'title', 'slug', 'text', 'published_at')


class FenaconMidiaSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = FenaconMidia
        fields = (
            'id', 'title', 'subtitle', 'slug', 'text', 'font', 'author',
            'published_at', 'featured_image', 'credit', 'user')


class SyndicateSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Syndicate
        fields = (
            'id', 'name', 'president', 'address', 'region', 'state', 'city',
            'cep', 'phone', 'email', 'page', 'code', 'status', 'created_at',
            'user')


class OfficeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Office
        fields = (
            'id', 'cnpj', 'company_name', 'patio_type', 'address', 'number',
            'complement', 'neighborhood', 'city', 'state', 'cep', 'emails',
            'phone')


class MagazineSerializer(serializers.ModelSerializer):
    class Meta:
        model = Magazine
        fields = ('id', 'number', 'image', 'file', 'type', 'created_at')


class AboutUsSerializer(serializers.ModelSerializer):
    class Meta:
        model = AboutUs
        fields = ('id', 'title', 'text', 'created_at')


class CustomAuthTokenSerializer(serializers.Serializer):
    username = serializers.CharField(label=_("Username"), required=False)
    google_token = serializers.CharField(label=_("Google token"), required=False)
    facebook_token = serializers.CharField(label=_("Facebook token"), required=False)
    password = serializers.CharField(label=_("Password"), style={'input_type': 'password'}, required=False)

    def validate(self, attrs):
        username = attrs.get('username')
        google_token = attrs.get('google_token')
        facebook_token = attrs.get('facebook_token')
        password = attrs.get('password')

        if username and password:
            user = authenticate(username=username, password=password)

            if user:
                if not user.is_active:
                    msg = _('User account is disabled.')
                    raise serializers.ValidationError(msg)
            else:
                msg = _('Unable to log in with provided credentials.')
                raise serializers.ValidationError(msg)
        elif google_token:
            try:
                user = User.objects.get(google_token=google_token)
            except User.DoesNotExist:
                raise serializers.ValidationError({
                    'non_field_errors': 'Nenhum usuário cadastrado com esta conta google.'
                })
        elif facebook_token:
            import facebook
            graph = facebook.GraphAPI(facebook_token)
            args = {'fields': 'id,name,email', }
            profile = graph.get_object('me', **args)
            try:
                user = User.objects.get(email=profile['email'])
            except User.DoesNotExist:
                raise serializers.ValidationError({
                    'non_field_errors': 'Nenhum usuário cadastrado com esta conta facebook.'
                })
        else:
            raise serializers.ValidationError({
                'non_field_errors': 'São obrigatórios google_token, facebook_token ou username e password.'
            })

        attrs['user'] = user
        return attrs
