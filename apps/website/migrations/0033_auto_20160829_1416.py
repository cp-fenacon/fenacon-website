# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import apps.website.models


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0032_auto_20160816_2000'),
    ]

    operations = [
        migrations.CreateModel(
            name='PromotionSubscriber',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=90, verbose_name=b'Nome')),
                ('phone', models.CharField(max_length=90, verbose_name=b'Telefone')),
                ('email', models.EmailField(max_length=254)),
                ('registration_number', models.CharField(max_length=200, verbose_name=b'N\xc3\xbamero de inscri\xc3\xa7\xc3\xa3o')),
            ],
            options={
                'verbose_name': 'Participante da promo\xe7\xe3o',
                'verbose_name_plural': 'Participantes da promo\xe7\xe3o',
            },
        ),
    ]
