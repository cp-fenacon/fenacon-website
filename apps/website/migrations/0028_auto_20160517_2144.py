# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0027_auto_20160130_1337'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='slug',
            field=models.SlugField(max_length=200, blank=True),
        ),
        migrations.AlterField(
            model_name='fenaconmidia',
            name='slug',
            field=models.SlugField(max_length=200, blank=True),
        ),
        migrations.AlterField(
            model_name='news',
            name='slug',
            field=models.SlugField(max_length=200, blank=True),
        ),
        migrations.AlterField(
            model_name='pressclipping',
            name='slug',
            field=models.SlugField(max_length=200, blank=True),
        ),
    ]
