# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor_uploader.fields


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0026_remove_fenaconmidia_featured'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='text',
            field=ckeditor_uploader.fields.RichTextUploadingField(null=True, verbose_name=b'Texto', blank=True),
        ),
        migrations.AlterField(
            model_name='fenaconmidia',
            name='text',
            field=ckeditor_uploader.fields.RichTextUploadingField(verbose_name=b'Texto'),
        ),
        migrations.AlterField(
            model_name='news',
            name='text',
            field=ckeditor_uploader.fields.RichTextUploadingField(verbose_name=b'Texto'),
        ),
        migrations.AlterField(
            model_name='pressclipping',
            name='text',
            field=ckeditor_uploader.fields.RichTextUploadingField(verbose_name=b'Texto'),
        ),
    ]
