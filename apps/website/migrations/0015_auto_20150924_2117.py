# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0014_auto_20150924_1804'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='author',
            field=models.CharField(default=None, help_text=b'Autor da not\xc3\xadcia', max_length=50, verbose_name=b'Autor', blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='news',
            name='font',
            field=models.CharField(help_text=b'Fonte da not\xc3\xadcia', max_length=50, verbose_name=b'Fonte', blank=True),
            preserve_default=True,
        ),
    ]
