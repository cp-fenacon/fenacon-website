# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0021_auto_20150926_1908'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='city',
            field=models.CharField(max_length=50, null=True, verbose_name=b'Cidade/Estado', blank=True),
        ),
        migrations.AlterField(
            model_name='event',
            name='region',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name=b'Regi\xc3\xa3o', choices=[(b'norte', b'Norte'), (b'centro-oeste', b'Centro-Oeste'), (b'sul', b'Sul'), (b'nordeste', b'Nordeste'), (b'sudeste', b'Sudeste')]),
        ),
        migrations.AlterField(
            model_name='event',
            name='text',
            field=models.TextField(null=True, verbose_name=b'Texto', blank=True),
        ),
    ]
