# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0023_remove_banner_photo_thumb'),
    ]

    operations = [
        migrations.CreateModel(
            name='PressClipping',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=90, verbose_name=b'T\xc3\xadtulo')),
                ('text', models.TextField(verbose_name=b'Texto')),
                ('slug', models.SlugField(unique=True, max_length=200, blank=True)),
                ('published_at', models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Data de Publicacao')),
            ],
            options={
                'verbose_name': 'Press Clipping',
                'verbose_name_plural': 'Press Clipping',
            },
        ),
    ]
