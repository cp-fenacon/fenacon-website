# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0003_auto_20150901_1741'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='region',
            field=models.CharField(default=None, max_length=10, verbose_name=b'Regi\xc3\xa3o', choices=[(b'1', b'Nordeste'), (b'2', b'Centro-Oeste'), (b'3', b'Sudeste'), (b'0', b'Norte'), (b'4', b'Sul')]),
            preserve_default=False,
        ),
    ]
