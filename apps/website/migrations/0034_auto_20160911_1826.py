# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0033_auto_20160829_1416'),
    ]

    operations = [
        migrations.AlterField(
            model_name='promotionsubscriber',
            name='name',
            field=models.CharField(max_length=90, verbose_name=b'nome'),
        ),
        migrations.AlterField(
            model_name='promotionsubscriber',
            name='phone',
            field=models.CharField(max_length=90, verbose_name=b'telefone'),
        ),
        migrations.AlterField(
            model_name='promotionsubscriber',
            name='registration_number',
            field=models.CharField(max_length=200, verbose_name=b'numero de inscricao'),
        ),
    ]
