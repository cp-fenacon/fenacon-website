# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings
import image_cropping.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Banner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('photo', models.ImageField(upload_to=b'uploads/banner/', null=True, verbose_name=b'Foto', blank=True)),
                (b'photo_thumb', image_cropping.fields.ImageRatioField(b'photo', '190x190', hide_image_field=False, size_warning=False, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=None, verbose_name='photo thumb')),
                ('description', models.TextField(help_text=b'Breve descri\xc3\xa7\xc3\xa3o da livro', null=True, verbose_name=b'Descri\xc3\xa7\xc3\xa3o', blank=True)),
                ('created_at', models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Data do Cadastro', blank=True)),
                ('status', models.BooleanField(default=True)),
                ('user', models.ForeignKey(verbose_name=b'Usu\xc3\xa1rio', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Banner',
                'verbose_name_plural': 'Banners',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Slide',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('photo', models.ImageField(help_text=b'Imagem para o slide da p\xc3\xa1gina inicial, tamanho 1200x400', upload_to=b'uploads/slide/', verbose_name=b'Foto')),
                ('link', models.URLField(help_text=b'Para redirecionar a outra p\xc3\xa1gina.', null=True, blank=True)),
                ('status', models.BooleanField(default=True, verbose_name=b'Ativo?')),
                ('created_at', models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Data do Cadastro', blank=True)),
                ('user', models.ForeignKey(verbose_name=b'Usu\xc3\xa1rio', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('created_at',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SocialNetwork',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.EmailField(max_length=75, null=True, blank=True)),
                ('facebook', models.URLField(null=True, blank=True)),
                ('instagram', models.URLField(null=True, blank=True)),
                ('twitter', models.URLField(null=True, blank=True)),
                ('youtube', models.URLField(null=True, blank=True)),
                ('snapwidget', models.TextField(help_text=b'Adicione o c\xc3\xb3digo gerado pelo site http://snapwidget.com, com layout: "4x2", responsive: "Yes"', null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Rede Social',
                'verbose_name_plural': 'Redes Sociais',
            },
            bases=(models.Model,),
        ),
    ]
