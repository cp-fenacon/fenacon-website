# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0029_contestsubscriber'),
    ]

    operations = [
        migrations.AddField(
            model_name='contestsubscriber',
            name='work_file',
            field=models.FileField(default=datetime.datetime(2016, 8, 4, 12, 28, 44, 589623, tzinfo=utc), upload_to=b'uploads/concurso/'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='contestsubscriber',
            name='file',
            field=models.FileField(upload_to=b'uploads/concurso/declaracao/'),
        ),
    ]
