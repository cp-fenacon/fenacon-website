# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0018_auto_20150924_2346'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='region',
            field=models.CharField(max_length=50, verbose_name=b'Regi\xc3\xa3o', choices=[(b'norte', b'Norte'), (b'centro-oeste', b'Centro-Oeste'), (b'sul', b'Sul'), (b'nordeste', b'Nordeste'), (b'sudeste', b'Sudeste')]),
        ),
        migrations.AlterField(
            model_name='socialnetwork',
            name='email',
            field=models.EmailField(max_length=254, null=True, verbose_name=b'Email da asessoria de impresa', blank=True),
        ),
    ]
