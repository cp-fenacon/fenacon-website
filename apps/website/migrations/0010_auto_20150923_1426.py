# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import image_cropping.fields


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0009_auto_20150923_1359'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='event',
            name=b'featured_crop',
        ),
        migrations.RemoveField(
            model_name='news',
            name=b'featured_small',
        ),
        migrations.AddField(
            model_name='event',
            name='featured',
            field=models.BooleanField(default=False, help_text=b'Coloca a not\xc3\xadcia em destaque. Deve possuir imagem destaque.', verbose_name=b'Destaque secund\xc3\xa1rio'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name=b'featured_big',
            field=image_cropping.fields.ImageRatioField(b'featured_image', '600x335', hide_image_field=False, size_warning=False, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=b'Destaque princial na home.', verbose_name=b'Destaque Grande'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='event',
            name=b'featured_medium',
            field=image_cropping.fields.ImageRatioField(b'featured_image', '470x180', hide_image_field=False, size_warning=False, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=b'Destaque secund\xc3\xa1rio home.', verbose_name=b'Destaque M\xc3\xa9dio'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='event',
            name='main_featured',
            field=models.BooleanField(default=False, help_text=b'Coloca a not\xc3\xadcia em destaque. Deve possuir imagem destaque.', verbose_name=b'Destaque principal'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='news',
            name='main_featured',
            field=models.BooleanField(default=False, help_text=b'Coloca a not\xc3\xadcia em destaque. Deve possuir imagem destaque.', verbose_name=b'Destaque principal'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='featured_image',
            field=models.ImageField(help_text=b'Dimens\xc3\xb5es 600x335 ou maior - JPEG', upload_to=b'uploads/eventos/%Y/%m/', verbose_name=b'Imagem Destaque'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='news',
            name='featured',
            field=models.BooleanField(default=False, help_text=b'Coloca a not\xc3\xadcia em destaque. Deve possuir imagem destaque.', verbose_name=b'Destaque secund\xc3\xa1rio'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='news',
            name=b'featured_big',
            field=image_cropping.fields.ImageRatioField(b'featured_image', '600x335', hide_image_field=False, size_warning=False, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=b'Destaque princial na home.', verbose_name=b'Destaque Grande'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='news',
            name='featured_image',
            field=models.ImageField(help_text=b'Dimens\xc3\xb5es 600x335px ou maior - JPEG', upload_to=b'uploads/noticias/%Y/%m/', verbose_name=b'Imagem Destaque', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='news',
            name=b'featured_medium',
            field=image_cropping.fields.ImageRatioField(b'featured_image', '470x180', hide_image_field=False, size_warning=False, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=b'Destaque secund\xc3\xa1rio home.', verbose_name=b'Destaque M\xc3\xa9dio'),
            preserve_default=True,
        ),
    ]
