# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0016_auto_20150924_2231'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='socialnetwork',
            name='city',
        ),
        migrations.RemoveField(
            model_name='socialnetwork',
            name='city_imprensa',
        ),
    ]
