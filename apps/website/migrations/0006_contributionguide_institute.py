# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('website', '0005_auto_20150922_1340'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContributionGuide',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('link', models.URLField(help_text=b'Endere\xc3\xa7o da p\xc3\xa1gina')),
                ('status', models.BooleanField(default=True, help_text=b'Se esta op\xc3\xa7\xc3\xa3o estiver desmarcada os usu\xc3\xa1rios do portal n\xc3\xa3o ir\xc3\xa3o mais ver este institui\xc3\xa7\xc3\xa3o.', verbose_name=b'Ativo?')),
                ('user', models.ForeignKey(verbose_name=b'Usu\xc3\xa1rio', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Guia de contribui\xe7\xe3o sindical',
                'verbose_name_plural': 'Guia de contribui\xe7\xe3o sindical',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Institute',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=90, verbose_name=b'Nome')),
                ('description', models.CharField(max_length=140, verbose_name=b'Descri\xc3\xa7\xc3\xa3o')),
                ('link', models.URLField(help_text=b'Endere\xc3\xa7o da p\xc3\xa1gina')),
                ('featured_image', models.ImageField(help_text=b'Dimens\xc3\xb5es 275x70px', upload_to=b'uploads/instituto/%Y/%m/', verbose_name=b'Imagem')),
                ('status', models.BooleanField(default=True, help_text=b'Se esta op\xc3\xa7\xc3\xa3o estiver desmarcada os usu\xc3\xa1rios do portal n\xc3\xa3o ir\xc3\xa3o mais ver este institui\xc3\xa7\xc3\xa3o.', verbose_name=b'Ativo?')),
                ('user', models.ForeignKey(verbose_name=b'Usu\xc3\xa1rio', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Instituto Fenacon',
                'verbose_name_plural': 'Institutos Fenacon',
            },
            bases=(models.Model,),
        ),
    ]
