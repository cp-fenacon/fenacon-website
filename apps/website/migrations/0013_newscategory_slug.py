# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0012_auto_20150924_1602'),
    ]

    operations = [
        migrations.AddField(
            model_name='newscategory',
            name='slug',
            field=models.SlugField(default=None, unique=True, max_length=200, blank=True),
            preserve_default=False,
        ),
    ]
