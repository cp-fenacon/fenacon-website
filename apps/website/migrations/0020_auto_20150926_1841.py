# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0019_auto_20150926_1838'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='featured_image',
            field=models.ImageField(help_text=b'Dimens\xc3\xb5es 600x335 ou maior - JPEG', upload_to=b'uploads/eventos/%Y/%m/', null=True, verbose_name=b'Imagem Destaque', blank=True),
        ),
    ]
