# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0030_auto_20160804_0928'),
    ]

    operations = [
        migrations.AddField(
            model_name='fenaconmidia',
            name='last_news',
            field=models.BooleanField(default=False, verbose_name=b'Adicionar a \xc3\xbaltimas not\xc3\xadcias?'),
        ),
    ]
