# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0008_auto_20150922_1622'),
    ]

    operations = [
        migrations.AddField(
            model_name='banner',
            name='link',
            field=models.URLField(help_text=b'Endere\xc3\xa7o da p\xc3\xa1gina', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='banner',
            name='photo',
            field=models.ImageField(help_text=b'Dimens\xc3\xb5es 870x135px', upload_to=b'uploads/banner/', null=True, verbose_name=b'Foto', blank=True),
            preserve_default=True,
        ),
    ]
