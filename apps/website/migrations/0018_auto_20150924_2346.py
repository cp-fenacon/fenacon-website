# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0017_auto_20150924_2232'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='city',
            field=models.CharField(default=None, max_length=50, verbose_name=b'Cidade/Estado'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='event',
            name='local',
            field=models.CharField(max_length=150, verbose_name=b'Endere\xc3\xa7o'),
            preserve_default=True,
        ),
    ]
