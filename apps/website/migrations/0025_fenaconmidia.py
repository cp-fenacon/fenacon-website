# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import image_cropping.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('website', '0024_pressclipping'),
    ]

    operations = [
        migrations.CreateModel(
            name='FenaconMidia',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(help_text=b'Titulo da Noticia', max_length=90, verbose_name=b'T\xc3\xadtulo')),
                ('subtitle', models.CharField(help_text=b'Para Manchetes', max_length=100, null=True, verbose_name=b'Sub-t\xc3\xadtulo', blank=True)),
                ('text', models.TextField(verbose_name=b'Texto')),
                ('slug', models.SlugField(unique=True, max_length=200, blank=True)),
                ('credit', models.CharField(help_text=b'Creditos da foto.', max_length=50, verbose_name=b'Cr\xc3\xa9dito', blank=True)),
                ('font', models.CharField(help_text=b'Fonte da not\xc3\xadcia', max_length=50, verbose_name=b'Fonte', blank=True)),
                ('author', models.CharField(help_text=b'Autor da not\xc3\xadcia', max_length=50, verbose_name=b'Autor', blank=True)),
                ('featured_image', models.ImageField(help_text=b'Dimens\xc3\xb5es 600x335px ou maior - JPEG', upload_to=b'uploads/noticias/%Y/%m/', verbose_name=b'Imagem Destaque', blank=True)),
                (b'featured_big', image_cropping.fields.ImageRatioField(b'featured_image', '600x335', hide_image_field=False, size_warning=False, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=b'Destaque princial na home.', verbose_name=b'Destaque Grande')),
                (b'featured_medium', image_cropping.fields.ImageRatioField(b'featured_image', '470x180', hide_image_field=False, size_warning=False, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=b'Destaque secund\xc3\xa1rio home.', verbose_name=b'Destaque M\xc3\xa9dio')),
                (b'featured_small', image_cropping.fields.ImageRatioField(b'featured_image', '170x170', hide_image_field=False, size_warning=False, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=b'Imagem para listagem de not\xc3\xadcias e \xc3\xbaltimas no\xc3\xadticas.', verbose_name=b'Destaque Pequeno')),
                ('published_at', models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Data de Publicacao')),
                ('status', models.BooleanField(default=True, help_text=b'Se esta op\xc3\xa7\xc3\xa3o estiver desmarcada os usu\xc3\xa1rios do portal n\xc3\xa3o ir\xc3\xa3o mais ver esta not\xc3\xadcia', verbose_name=b'Noticia Ativa?')),
                ('featured', models.BooleanField(default=False, help_text=b'Coloca a not\xc3\xadcia em destaque. Deve possuir imagem destaque.', verbose_name=b'Destaque secund\xc3\xa1rio')),
                ('user', models.ForeignKey(verbose_name=b'Usu\xc3\xa1rio', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('-published_at',),
                'get_latest_by': 'published_at',
                'verbose_name': 'FENACON na M\xeddia',
                'verbose_name_plural': 'FENACON na M\xeddia',
            },
        ),
    ]
