# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import image_cropping.fields


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0013_newscategory_slug'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name=b'featured_small',
            field=image_cropping.fields.ImageRatioField(b'featured_image', '170x170', hide_image_field=False, size_warning=False, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=b'Imagem para listagem de eventos.', verbose_name=b'Destaque Pequeno'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='event',
            name='region',
            field=models.CharField(max_length=10, verbose_name=b'Regi\xc3\xa3o', choices=[(b'norte', b'Norte'), (b'centro-oeste', b'Centro-Oeste'), (b'sul', b'Sul'), (b'nordeste', b'Nordeste'), (b'sudeste', b'Sudeste')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='news',
            name=b'featured_small',
            field=image_cropping.fields.ImageRatioField(b'featured_image', '170x170', hide_image_field=False, size_warning=False, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=b'Imagem para listagem de not\xc3\xadcias e \xc3\xbaltimas no\xc3\xadticas.', verbose_name=b'Destaque Pequeno'),
            preserve_default=True,
        ),
    ]
