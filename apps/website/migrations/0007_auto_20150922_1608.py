# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0006_contributionguide_institute'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='socialnetwork',
            options={'verbose_name': 'Contato', 'verbose_name_plural': 'Contato'},
        ),
        migrations.RemoveField(
            model_name='socialnetwork',
            name='snapwidget',
        ),
        migrations.AddField(
            model_name='socialnetwork',
            name='address',
            field=models.CharField(max_length=200, null=True, verbose_name=b'Endere\xc3\xa7o', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='socialnetwork',
            name='telefax',
            field=models.CharField(max_length=30, null=True, blank=True),
            preserve_default=True,
        ),
    ]
