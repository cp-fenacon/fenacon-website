# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc
from django.conf import settings
import image_cropping.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('website', '0002_remove_banner_description'),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(help_text=b'Titulo do Evento', max_length=90, verbose_name=b'T\xc3\xadtulo')),
                ('subtitle', models.CharField(help_text=b'Para Manchetes', max_length=100, verbose_name=b'Sub-t\xc3\xadtulo')),
                ('text', models.TextField(verbose_name=b'Texto')),
                ('slug', models.SlugField(unique=True, max_length=200, blank=True)),
                ('credit', models.CharField(help_text=b'Creditos da foto.', max_length=30, verbose_name=b'Cr\xc3\xa9dito', blank=True)),
                ('featured_image', models.ImageField(help_text=b'Foto destaque dentro do evento. Dimens\xc3\xb5es 300x300px - JPEG - 72dpi.', upload_to=b'uploads/eventos/%Y/%m/', verbose_name=b'Imagem Destaque')),
                (b'featured_crop', image_cropping.fields.ImageRatioField(b'featured_image', '253x253', hide_image_field=False, size_warning=False, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=b'Destaque para home e central de eventos.', verbose_name=b'Destaque M\xc3\xa9dio')),
                ('published_at', models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Data de Publicacao')),
                ('start_at', models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Come\xc3\xa7a em')),
                ('ends_in', models.DateTimeField(verbose_name=b'T\xc3\xa9rmina em')),
                ('status', models.BooleanField(default=True, help_text=b'Se esta op\xc3\xa7\xc3\xa3o estiver desmarcada os usu\xc3\xa1rios do portal n\xc3\xa3o ir\xc3\xa3o mais ver este evento.', verbose_name=b'Evento Ativo?')),
                ('user', models.ForeignKey(verbose_name=b'Usu\xc3\xa1rio', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('-published_at',),
                'get_latest_by': 'published_at',
                'verbose_name': 'Evento',
                'verbose_name_plural': 'Eventos',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(help_text=b'Titulo da Noticia', max_length=90, verbose_name=b'T\xc3\xadtulo')),
                ('subtitle', models.CharField(help_text=b'Para Manchetes', max_length=100, null=True, verbose_name=b'Sub-t\xc3\xadtulo', blank=True)),
                ('text', models.TextField(verbose_name=b'Texto')),
                ('slug', models.SlugField(unique=True, max_length=200, blank=True)),
                ('credit', models.CharField(help_text=b'Creditos da foto.', max_length=30, verbose_name=b'Cr\xc3\xa9dito', blank=True)),
                ('featured_image', models.ImageField(help_text=b'Dimens\xc3\xb5es 570x356px - JPEG - 72dpi', upload_to=b'uploads/noticias/%Y/%m/', verbose_name=b'Imagem Destaque', blank=True)),
                (b'featured_big', image_cropping.fields.ImageRatioField(b'featured_image', '570x356', hide_image_field=False, size_warning=False, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=b'Destaque slide na central de not\xc3\xadcias.', verbose_name=b'Destaque Grande')),
                (b'featured_medium', image_cropping.fields.ImageRatioField(b'featured_image', '235x235', hide_image_field=False, size_warning=False, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=b'Destaque para home.', verbose_name=b'Destaque M\xc3\xa9dio')),
                (b'featured_small', image_cropping.fields.ImageRatioField(b'featured_image', '185x185', hide_image_field=False, size_warning=False, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=b'Imagem para mais destaques na central de not\xc3\xadcias.', verbose_name=b'Destaque Pequeno')),
                ('published_at', models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Data de Publicacao')),
                ('status', models.BooleanField(default=True, help_text=b'Se esta op\xc3\xa7\xc3\xa3o estiver desmarcada os usu\xc3\xa1rios do portal n\xc3\xa3o ir\xc3\xa3o mais ver esta not\xc3\xadcia', verbose_name=b'Noticia Ativa?')),
                ('featured', models.BooleanField(default=False, help_text=b'Coloca a not\xc3\xadcia em destaque. Deve possuir imagem destaque.', verbose_name=b'Destaque')),
            ],
            options={
                'ordering': ('-published_at',),
                'get_latest_by': 'published_at',
                'verbose_name': 'Not\xedcia',
                'verbose_name_plural': 'Not\xedcias',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='NewsCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(help_text=b'Titulo da Categoria', max_length=90, verbose_name=b'T\xc3\xadtulo')),
                ('created_at', models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Data do Cadastro', blank=True)),
                ('status', models.BooleanField(default=True)),
                ('user', models.ForeignKey(verbose_name=b'Usu\xc3\xa1rio', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('-created_at',),
                'get_latest_by': 'created_at',
                'verbose_name': 'Categoria de Not\xedcia',
                'verbose_name_plural': 'Categorias de Not\xedcias',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='news',
            name='category',
            field=models.ForeignKey(verbose_name=b'Categoria', to='website.NewsCategory', help_text=b'Categoria que a not\xc3\xadcia pertence.'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='news',
            name='user',
            field=models.ForeignKey(verbose_name=b'Usu\xc3\xa1rio', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='banner',
            name='ends_in',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 1, 20, 41, 54, 808566, tzinfo=utc), verbose_name=b'T\xc3\xa9rmina em'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='banner',
            name='start_at',
            field=models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Come\xc3\xa7a em'),
            preserve_default=True,
        ),
    ]
