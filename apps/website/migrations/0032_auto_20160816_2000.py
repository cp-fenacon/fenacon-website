# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import apps.website.models


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0031_fenaconmidia_last_news'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='fenaconmidia',
            name='last_news',
        ),
        migrations.AddField(
            model_name='news',
            name='fenacon_midia',
            field=models.BooleanField(default=False, help_text=b'Coloca a not\xc3\xadcia na listagem de FENACON na m\xc3\xaddia.', verbose_name=b'FENACON na m\xc3\xaddia'),
        ),
    ]
