# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0020_auto_20150926_1841'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='newscategory',
            options={'verbose_name': 'Categoria de Not\xedcia', 'verbose_name_plural': 'Categorias de Not\xedcias'},
        ),
        migrations.RemoveField(
            model_name='contributionguide',
            name='user',
        ),
        migrations.RemoveField(
            model_name='newscategory',
            name='created_at',
        ),
        migrations.RemoveField(
            model_name='newscategory',
            name='user',
        ),
    ]
