# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0035_auto_20161211_1514'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='is_mobile',
            field=models.BooleanField(default=True, help_text=b'Se esta op\xc3\xa7\xc3\xa3o estiver desmarcada os usu\xc3\xa1rios do app n\xc3\xa3o ir\xc3\xa3o mais ver esta not\xc3\xadcia', verbose_name=b'Habilitar no app?'),
        ),
    ]
