# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import image_cropping.fields


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0010_auto_20150923_1426'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name=b'featured_small',
            field=image_cropping.fields.ImageRatioField(b'featured_image', '100x100', hide_image_field=False, size_warning=False, allow_fullsize=False, free_crop=False, adapt_rotation=False, help_text=b'Imagem para mais destaques na central de not\xc3\xadcias.', verbose_name=b'Destaque Pequeno'),
            preserve_default=False,
        ),
    ]
