# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0028_auto_20160517_2144'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContestSubscriber',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=140, verbose_name=b'Nome')),
                ('cpf', models.CharField(max_length=15, verbose_name=b'CPF')),
                ('address', models.CharField(max_length=200, verbose_name=b'Endere\xc3\xa7o')),
                ('cep', models.CharField(max_length=200, verbose_name=b'CEP')),
                ('phone', models.CharField(max_length=200, verbose_name=b'Telefone')),
                ('email', models.EmailField(max_length=254)),
                ('file', models.FileField(upload_to=b'')),
            ],
            options={
                'verbose_name': 'Inscri\xe7\xe3o de concurso',
                'verbose_name_plural': 'Inscri\xe7\xf5es de concurso',
            },
        ),
    ]
