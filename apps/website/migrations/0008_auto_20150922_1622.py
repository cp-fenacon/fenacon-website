# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0007_auto_20150922_1608'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='slide',
            name='user',
        ),
        migrations.DeleteModel(
            name='Slide',
        ),
    ]
