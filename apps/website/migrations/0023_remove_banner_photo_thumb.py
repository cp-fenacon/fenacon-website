# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0022_auto_20151001_2242'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='banner',
            name=b'photo_thumb',
        ),
    ]
