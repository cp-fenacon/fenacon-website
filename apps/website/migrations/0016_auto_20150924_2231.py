# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0015_auto_20150924_2117'),
    ]

    operations = [
        migrations.AddField(
            model_name='socialnetwork',
            name='address_imprensa',
            field=models.CharField(max_length=200, null=True, verbose_name=b'Endere\xc3\xa7o Assessoria', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='socialnetwork',
            name='cep',
            field=models.CharField(max_length=200, null=True, verbose_name=b'CEP da FENACON', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='socialnetwork',
            name='cep_imprensa',
            field=models.CharField(max_length=200, null=True, verbose_name=b'CEP Assessoria', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='socialnetwork',
            name='city',
            field=models.CharField(max_length=200, null=True, verbose_name=b'Cidade FENACON', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='socialnetwork',
            name='city_imprensa',
            field=models.CharField(max_length=200, null=True, verbose_name=b'Cidade Assessoria', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='socialnetwork',
            name='phone',
            field=models.CharField(max_length=30, null=True, verbose_name=b'Telefones Assessoria', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='socialnetwork',
            name='address',
            field=models.CharField(max_length=200, null=True, verbose_name=b'Endere\xc3\xa7o FENACON', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='socialnetwork',
            name='email',
            field=models.EmailField(max_length=75, null=True, verbose_name=b'Email da asessoria de impresa', blank=True),
            preserve_default=True,
        ),
    ]
