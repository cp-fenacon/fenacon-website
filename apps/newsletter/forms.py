# -*- coding: utf-8 -*-
from django import forms
from apps.newsletter.models import Subscriber


class SubscriberForm(forms.ModelForm):
    class Meta:
        model = Subscriber
        fields = ['email', 'name']
