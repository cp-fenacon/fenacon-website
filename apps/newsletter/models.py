# coding=utf-8
from datetime import datetime
from django.db import models

class Subscriber(models.Model):
    name = models.CharField("name", max_length=100)
    email = models.EmailField("email", unique=True)
    active = models.BooleanField("Ativo", default=True)

    def __unicode__(self):
        return self.email

    class Meta:
        verbose_name = "Assinante"
        verbose_name_plural = "Assinantes"


class Newsletter(models.Model):
    title = models.CharField('Assunto', max_length=50)
    creation_date = models.DateTimeField('Data de criação', default=datetime.now)
    content = models.TextField('Conteúdo')

    class Meta:
        verbose_name = 'Newsletter'
        verbose_name_plural = 'Newsletters'

    def __unicode__(self):
        return self.title