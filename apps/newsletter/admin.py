from ckeditor.widgets import CKEditorWidget
from django.contrib import admin
from django.db import models
from apps.newsletter.models import Subscriber, Newsletter
from django.core.mail import EmailMultiAlternatives
from fenacon import settings


def send_newsletter(ModelAdmin, request, queryset):
    selected = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
    newsletters = Newsletter.objects.filter(id__in=selected)
    contacts = Subscriber.objects.filter(active=True, email__isnull=False).values_list('email', flat=True)
    for newsletter in newsletters:
        from_email = settings.EMAIL_HOST_USER
        msg = EmailMultiAlternatives(newsletter.title, newsletter.content, from_email, contacts)
        msg.content_subtype = "html"
        msg.send()
    ModelAdmin.message_user(request, "Newsletters enviadas com sucesso.")

send_newsletter.short_description = "Enviar newsletters selecionadas"


def ativar(ModelAdmin, request, queryset):
    queryset.update(active=True)


def desativar(ModelAdmin, request, queryset):
    queryset.update(active=False)


class SubscriberAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'active')
    list_filter = ['active']
    actions = [ativar, desativar]


class NewsletterAdmin(admin.ModelAdmin):
    list_display = ('title', 'creation_date')
    list_filter = ['creation_date']
    search_fields = ['title']
    actions = [send_newsletter]
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget()}
    }


admin.site.register(Newsletter, NewsletterAdmin)
admin.site.register(Subscriber, SubscriberAdmin)