# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('newsletter', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='newsletter',
            name='content',
            field=ckeditor.fields.RichTextField(verbose_name=b'Conte\xc3\xbado'),
            preserve_default=True,
        ),
    ]
