# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('newsletter', '0002_auto_20150901_1710'),
    ]

    operations = [
        migrations.AlterField(
            model_name='newsletter',
            name='content',
            field=models.TextField(verbose_name=b'Conte\xc3\xbado'),
            preserve_default=True,
        ),
    ]
