# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Newsletter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50, verbose_name=b'Assunto')),
                ('creation_date', models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Data de cria\xc3\xa7\xc3\xa3o')),
                ('content', models.TextField(verbose_name=b'Conte\xc3\xbado')),
            ],
            options={
                'verbose_name': 'Newsletter',
                'verbose_name_plural': 'Newsletters',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Subscriber',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.EmailField(unique=True, max_length=75, verbose_name=b'email')),
                ('active', models.BooleanField(default=True, verbose_name=b'Ativo')),
            ],
            options={
                'verbose_name': 'Assinante',
                'verbose_name_plural': 'Assinantes',
            },
            bases=(models.Model,),
        ),
    ]
