from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from apps.accounts.models import User


UserAdmin.fieldsets += (('Social Tokens', {'fields': ('google_token', 'facebook_token')}),)
admin.site.register(User, UserAdmin)
