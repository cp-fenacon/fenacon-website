# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_auto_20161221_1607'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='facebook_token',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='google_token',
            field=models.TextField(null=True, blank=True),
        ),
    ]
