# coding=utf-8
from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    google_token = models.TextField(null=True, blank=True)
    facebook_token = models.TextField(null=True, blank=True)

    class Meta:
        db_table = 'auth_user'


User._meta.get_field('username').max_length = 70
User._meta.get_field('username').help_text = 'Obrigatório. 70 caracteres ou menos. Somente letras, dígitos e @/./+/-/_.'
