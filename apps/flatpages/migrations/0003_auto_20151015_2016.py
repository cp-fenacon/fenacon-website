# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('flatpages', '0002_auto_20150923_1801'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='acting',
            options={'ordering': ('order',), 'get_latest_by': 'created_at', 'verbose_name': 'Atua\xe7\xe3o', 'verbose_name_plural': 'Atua\xe7\xf5es'},
        ),
        migrations.AlterModelOptions(
            name='institutional',
            options={'ordering': ('order',), 'get_latest_by': 'created_at', 'verbose_name': 'Institucional', 'verbose_name_plural': 'Institucionais'},
        ),
        migrations.AddField(
            model_name='acting',
            name='order',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='institutional',
            name='order',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
