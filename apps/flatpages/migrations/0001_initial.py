# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Acting',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(help_text=b'Titulo da Categoria', max_length=90, verbose_name=b'T\xc3\xadtulo')),
                ('text', models.TextField(verbose_name=b'Texto')),
                ('created_at', models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Data do Cadastro', blank=True)),
                ('status', models.BooleanField(default=True)),
                ('user', models.ForeignKey(verbose_name=b'Usu\xc3\xa1rio', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('-created_at',),
                'get_latest_by': 'created_at',
                'verbose_name': 'Atua\xe7\xe3o',
                'verbose_name_plural': 'Atua\xe7\xf5es',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Institutional',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(help_text=b'Titulo da Categoria', max_length=90, verbose_name=b'T\xc3\xadtulo')),
                ('text', models.TextField(verbose_name=b'Texto')),
                ('created_at', models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Data do Cadastro', blank=True)),
                ('status', models.BooleanField(default=True)),
                ('user', models.ForeignKey(verbose_name=b'Usu\xc3\xa1rio', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('-created_at',),
                'get_latest_by': 'created_at',
                'verbose_name': 'Institucional',
                'verbose_name_plural': 'Institucionais',
            },
            bases=(models.Model,),
        ),
    ]
