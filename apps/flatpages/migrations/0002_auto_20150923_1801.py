# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('flatpages', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='acting',
            name='slug',
            field=models.SlugField(default=None, unique=True, max_length=200, blank=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='institutional',
            name='slug',
            field=models.SlugField(default=None, unique=True, max_length=200, blank=True),
            preserve_default=False,
        ),
    ]
