# -*- coding: utf-8 -*-
from ckeditor.widgets import CKEditorWidget
from django import forms
from apps.flatpages.models import Institutional, Acting, Announce, AboutUs


class ActingForm(forms.ModelForm):
    text = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Acting
        fields = ('text',)


class InstitutionalForm(forms.ModelForm):
    text = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Institutional
        fields = ('text',)


class AnnounceForm(forms.ModelForm):
    text = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Announce
        fields = ('text',)


class AboutUsForm(forms.ModelForm):
    text = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = AboutUs
        fields = ('text',)
