from django.views.generic import DetailView
from apps.flatpages.models import Acting, Institutional, Announce


class ActingDetailView(DetailView):
    model = Acting
    template_name = 'flat-page.html'


class InstitutionalDetailView(DetailView):
    model = Institutional
    template_name = 'flat-page.html'


class AnnounceView(DetailView):
    model = Announce
    template_name = 'flat-page.html'
