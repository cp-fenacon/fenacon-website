# coding=utf-8
from adminsortable2.admin import SortableAdminMixin
from django.contrib import admin
from apps.flatpages.forms import (ActingForm, InstitutionalForm, AnnounceForm,
                                  AboutUsForm)
from apps.flatpages.models import Acting, Institutional, Announce, AboutUs


class ActingAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = ('title', 'user', 'created_at', 'status')
    list_filter = ['status']
    search_fields = ['title', 'text']
    form = ActingForm
    fieldsets = [
        (None, {'fields': ['title', 'text']}),
        (u'Informações', {'fields': ['status']}),
    ]

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()


class InstitutionalAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = ('title', 'user', 'created_at', 'status')
    list_filter = ['status']
    search_fields = ['title', 'text']
    form = InstitutionalForm
    fieldsets = [
        (None, {'fields': ['title', 'text']}),
        (u'Informações', {'fields': ['status']}),
    ]

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()


class AnnounceAdmin(admin.ModelAdmin):
    list_display = ('title', 'user', 'created_at', 'status')
    list_filter = ['status']
    search_fields = ['title', 'text']
    form = AnnounceForm
    fieldsets = [
        (None, {'fields': ['title', 'text']}),
        (u'Informações', {'fields': ['status']}),
    ]

    def has_add_permission(self, request):
        return False if self.model.objects.count() > 0 else True

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()


class AboutUsAdmin(admin.ModelAdmin):
    list_display = ('title', 'user', 'created_at')
    search_fields = ['title', 'text']
    form = AboutUsForm
    fieldsets = [
        (None, {'fields': ['title', 'text']}),
    ]

    def has_add_permission(self, request):
        return False if self.model.objects.count() > 0 else True

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()


admin.site.register(AboutUs, AboutUsAdmin)
admin.site.register(Announce, AnnounceAdmin)
admin.site.register(Acting, ActingAdmin)
admin.site.register(Institutional, InstitutionalAdmin)
