# coding=utf-8
from datetime import datetime
from apps.accounts.models import User
from django.db import models
from django.db.models import signals
from django.template.defaultfilters import slugify


class Acting(models.Model):
    title = models.CharField("Título", max_length=90, help_text="Titulo da Categoria")
    text = models.TextField(verbose_name="Texto")
    created_at = models.DateTimeField(verbose_name='Data do Cadastro', blank=True, default=datetime.now)
    user = models.ForeignKey(User, verbose_name='Usuário')
    status = models.BooleanField(default=True)
    slug = models.SlugField(max_length=200, blank=True, unique=True)
    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta:
        ordering = ('order',)
        verbose_name = u'Atuação'
        verbose_name_plural = u'Atuações'
        get_latest_by = 'created_at'

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return ('acting_detail', [self.slug, self.pk])


class Institutional(models.Model):
    title = models.CharField("Título", max_length=90, help_text="Titulo da Categoria")
    text = models.TextField(verbose_name="Texto")
    created_at = models.DateTimeField(verbose_name='Data do Cadastro', blank=True, default=datetime.now)
    user = models.ForeignKey(User, verbose_name='Usuário')
    status = models.BooleanField(default=True)
    slug = models.SlugField(max_length=200, blank=True, unique=True)
    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta:
        ordering = ('order',)
        verbose_name = u'Institucional'
        verbose_name_plural = u'Institucionais'
        get_latest_by = 'created_at'

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return ('institutional_detail', [self.slug, self.pk])


class AboutUs(models.Model):
    title = models.CharField("Título", max_length=90, help_text="Titulo da Categoria")
    text = models.TextField(verbose_name="Texto")
    created_at = models.DateTimeField(verbose_name='Data do Cadastro', blank=True, default=datetime.now)
    user = models.ForeignKey(User, verbose_name='Usuário')

    class Meta:
        verbose_name = u'Quem somos(aplicativo)'
        verbose_name_plural = u'Quem somos(aplicativo)'

    def __unicode__(self):
        return self.title


class Announce(models.Model):
    title = models.CharField("Título", max_length=90, help_text="Titulo da Categoria")
    text = models.TextField(verbose_name="Texto")
    created_at = models.DateTimeField(verbose_name='Data do Cadastro', blank=True, default=datetime.now)
    user = models.ForeignKey(User, verbose_name='Usuário')
    status = models.BooleanField(default=True)
    slug = models.SlugField(max_length=200, blank=True, unique=True)

    class Meta:
        verbose_name = u'Como anunciar'
        verbose_name_plural = u'Como anunciar'

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return ('announce_detail', [self.slug])


def announce_pre_save(signal, instance, sender, **kwargs):
    if not instance.slug:
        instance.slug = slugify(instance.title)


def acting_pre_save(signal, instance, sender, **kwargs):
    if not instance.slug:
        instance.slug = slugify(instance.title)


def institutional_pre_save(signal, instance, sender, **kwargs):
    if not instance.slug:
        instance.slug = slugify(instance.title)


signals.pre_save.connect(announce_pre_save, sender=Announce)
signals.pre_save.connect(acting_pre_save, sender=Acting)
signals.pre_save.connect(institutional_pre_save, sender=Institutional)