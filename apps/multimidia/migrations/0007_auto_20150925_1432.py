# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('multimidia', '0006_auto_20150925_1426'),
    ]

    operations = [
        migrations.AddField(
            model_name='categoryvideo',
            name='status',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='video',
            name='embed_code',
            field=models.CharField(help_text=b'C\xc3\xb3digo no endere\xc3\xa7o do v\xc3\xaddeo.Exemplo entre aspas: https://vimeo.com/"118149618" ou https://www.youtube.com/watch?v="nFD7Lqsy5Ro"', max_length=100, verbose_name=b'C\xc3\xb3digo do v\xc3\xaddeo'),
            preserve_default=True,
        ),
    ]
