# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('multimidia', '0005_auto_20150925_1416'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='video',
            name='url',
        ),
        migrations.AddField(
            model_name='video',
            name='title',
            field=models.CharField(default=None, help_text=b'Titulo do V\xc3\xaddeo', max_length=90, verbose_name=b'T\xc3\xadtulo'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='video',
            name='embed_code',
            field=models.CharField(help_text=b'C\xc3\xb3digo no endere\xc3\xa7o do v\xc3\xaddeo.Exemplo entre aspas: \xc3\x9fhttps://vimeo.com/"118149618" ou https://www.youtube.com/watch?v="nFD7Lqsy5Ro"', max_length=100, verbose_name=b'C\xc3\xb3digo do v\xc3\xaddeo'),
            preserve_default=True,
        ),
    ]
