# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('multimidia', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='magazine',
            name='image',
            field=models.ImageField(default=None, help_text=b'Imagem da capa da revista para exibi\xc3\xa7\xc3\xa3o na listagem.', verbose_name=b'Foto de capa', upload_to=b'uploads/revistas/capas/'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='magazine',
            name='file',
            field=models.FileField(upload_to=b'uploads/revistas/', verbose_name=b'Arquivo da revista'),
            preserve_default=True,
        ),
    ]
