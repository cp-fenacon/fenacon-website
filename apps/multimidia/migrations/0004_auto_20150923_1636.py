# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('multimidia', '0003_auto_20150923_1331'),
    ]

    operations = [
        migrations.AddField(
            model_name='magazine',
            name='type',
            field=models.CharField(default=None, max_length=2, verbose_name=b'Tipo', choices=[(b'0', b'Revista FENACON'), (b'1', b'Outras Publica\xc3\xa7\xc3\xb5es')]),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='magazine',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Data do Cadastro', blank=True),
            preserve_default=True,
        ),
    ]
