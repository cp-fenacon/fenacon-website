# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('multimidia', '0009_video_main_featured'),
    ]

    operations = [
        migrations.RenameField(
            model_name='video',
            old_name='created_at',
            new_name='published_at',
        ),
    ]
