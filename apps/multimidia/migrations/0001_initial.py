# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='CategoryVideo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name=b'Nome')),
                ('created_at', models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Data do Cadastro', blank=True)),
                ('user', models.ForeignKey(verbose_name=b'Usu\xc3\xa1rio', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Categoria de V\xeddeo',
                'verbose_name_plural': 'Categorias de V\xeddeos',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Magazine',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.IntegerField(verbose_name=b'N\xc3\xbamero')),
                ('file', models.FileField(upload_to=b'', verbose_name=b'Arquivo da revista')),
                ('status', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Data do Cadastro', blank=True)),
                ('user', models.ForeignKey(verbose_name=b'Usu\xc3\xa1rio', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Revista',
                'verbose_name_plural': 'Revistas',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.URLField()),
                ('embed_code', models.CharField(max_length=100)),
                ('type', models.CharField(max_length=2, verbose_name=b'Tipo', choices=[(b'0', b'Vimeo'), (b'1', b'Youtube')])),
                ('status', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Data do Cadastro', blank=True)),
                ('category', models.ForeignKey(verbose_name=b'Categoria', to='multimidia.CategoryVideo')),
                ('user', models.ForeignKey(verbose_name=b'Usu\xc3\xa1rio', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'V\xeddeo',
                'verbose_name_plural': 'V\xeddeos',
            },
            bases=(models.Model,),
        ),
    ]
