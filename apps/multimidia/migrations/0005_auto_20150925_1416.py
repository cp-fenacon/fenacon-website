# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('multimidia', '0004_auto_20150923_1636'),
    ]

    operations = [
        migrations.AddField(
            model_name='categoryvideo',
            name='slug',
            field=models.SlugField(default=None, unique=True, max_length=200, blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='magazine',
            name='number',
            field=models.CharField(max_length=50, verbose_name=b'Nome'),
            preserve_default=True,
        ),
    ]
