# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('multimidia', '0008_auto_20150928_1205'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='main_featured',
            field=models.BooleanField(default=False, help_text=b'Coloca o v\xc3\xaddeo em destaque na p\xc3\xa1gina inicial.', verbose_name=b'Destaque principal'),
        ),
    ]
