# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('multimidia', '0002_auto_20150923_1325'),
    ]

    operations = [
        migrations.AlterField(
            model_name='magazine',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Da\xc3\x9fta do Cadastro', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='magazine',
            name='number',
            field=models.CharField(max_length=50, verbose_name=b'N\xc3\xbamero da revista'),
            preserve_default=True,
        ),
    ]
