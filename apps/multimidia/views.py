from django.shortcuts import render
from django.views.generic import ListView, DetailView
from apps.multimidia.models import Magazine, Video, CategoryVideo


class MagazineListView(ListView):
    model = Magazine
    template_name = 'multimidia/publicacoes.html'
    paginate_by = 12

    def get_queryset(self):
        object_list = Magazine.objects.filter(status=True, type='0').order_by('-number')
        return object_list


class PublicationListView(ListView):
    model = Magazine
    template_name = 'multimidia/publicacoes.html'
    paginate_by = 12

    def get_queryset(self):
        object_list = Magazine.objects.filter(status=True, type='1').order_by('-created_at')
        return object_list


class VideoListView(ListView):
    model = Video
    paginate_by = 4
    template_name = 'multimidia/videos.html'

    def get_context_data(self, **kwargs):
        context = super(VideoListView, self).get_context_data(**kwargs)
        context['categories'] = CategoryVideo.objects.filter(status=True)
        return context

    def get_queryset(self):
        category = self.request.GET.get('category')
        object_list = Video.objects.filter(status=True).order_by('-published_at')
        if category:
            object_list = object_list.filter(category__slug=category).order_by('-published_at')

        return object_list


class VideoDetailView(DetailView):
    model = Video
    template_name = 'multimidia/interna-video.html'