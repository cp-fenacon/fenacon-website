# coding=utf-8
from django.contrib import admin
from apps.multimidia.models import Magazine, CategoryVideo, Video, File


class VideoAdmin(admin.ModelAdmin):
    list_display = ('title', 'user', 'published_at', 'status')
    list_filter = ['status', 'type']
    search_fields = ['title']
    fieldsets = [
        (None, {'fields': ['title', 'category', 'embed_code', 'type']}),
        (u'Informações', {'fields': ['status', 'main_featured']}),
    ]

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()


class MagazineAdmin(admin.ModelAdmin):
    list_display = ('number', 'show_image', 'user', 'created_at', 'status')
    list_filter = ['status', 'type']
    search_fields = ['number']

    def show_image(self, obj):
        if obj.image:
            return u'<img src="%s" width="50px" heigth="50px"/>' % obj.image.url
        else:
            return u''
    show_image.allow_tags = True
    show_image.short_description = u'Imagem'

    fieldsets = [
        (None, {'fields': ['number', 'type']}),
        ('Arquivos', {'fields': ['image', 'file']}),
        (u'Informações', {'fields': ['status']}),
    ]

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()


admin.site.register(Video, VideoAdmin)
admin.site.register(CategoryVideo)
admin.site.register(Magazine, MagazineAdmin)
admin.site.register(File)
