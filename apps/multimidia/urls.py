from django.conf.urls import patterns, url
from apps.multimidia.views import MagazineListView, PublicationListView, VideoListView, VideoDetailView

urlpatterns = patterns('',
    url(r'^videos/$', VideoListView.as_view(), name='videos'),
    url(r'^videos/(?P<slug>[\w_-]+)-(?P<pk>\d+)/$', VideoDetailView.as_view(), name='video_detail'),
    url(r'^revistas/$', MagazineListView.as_view(), name='magazines'),
    url(r'^outras-publicacoes/$', PublicationListView.as_view(), name='publications'),
)
