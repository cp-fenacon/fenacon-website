# -*- coding: utf-8 -*-
from datetime import datetime
from apps.accounts.models import User
from django.db import models
from django.db.models import signals
from django.template.defaultfilters import slugify
import os
import random
import string

from fenacon import settings


class CategoryVideo(models.Model):
    name = models.CharField('Nome', max_length=100)
    slug = models.SlugField(max_length=200, blank=True, unique=True)
    status = models.BooleanField(default=True)

    class Meta:
        verbose_name = u'Categoria de Vídeo'
        verbose_name_plural = u'Categorias de Vídeos'

    def __unicode__(self):
        return self.name


class Video(models.Model):
    title = models.CharField("Título", max_length=90, help_text="Titulo do Vídeo")
    slug = models.SlugField(max_length=200, blank=True)
    category = models.ForeignKey(CategoryVideo, verbose_name='Categoria')
    embed_code = models.CharField("Código do vídeo", max_length=100, help_text='Código no endereço do vídeo.Exemplo entre aspas: https://vimeo.com/"118149618" ou https://www.youtube.com/watch?v="nFD7Lqsy5Ro"')
    type = models.CharField('Tipo', max_length=2, choices={('0', 'Vimeo'), ('1', 'Youtube')})
    status = models.BooleanField(default=True)
    published_at = models.DateTimeField(verbose_name='Data do Cadastro', blank=True, default=datetime.now)
    user = models.ForeignKey(User, verbose_name='Usuário')
    main_featured = models.BooleanField(verbose_name='Destaque principal', default=False, help_text="Coloca o vídeo em destaque na página inicial.")

    class Meta:
        verbose_name = u'Vídeo'
        verbose_name_plural = u'Vídeos'

    @models.permalink
    def get_absolute_url(self):
        return ('video_detail', [self.slug, self.pk])

    def __unicode__(self):
        return self.title


class Magazine(models.Model):
    number = models.CharField('Nome', max_length=50)
    image = models.ImageField("Foto de capa", upload_to='uploads/revistas/capas/', help_text="Imagem da capa da revista para exibição na listagem.")
    file = models.FileField('Arquivo da revista', upload_to='uploads/revistas/')
    type = models.CharField('Tipo', max_length=2, choices={('0', 'Revista FENACON'), ('1', 'Outras Publicações')})
    status = models.BooleanField(default=True)
    created_at = models.DateTimeField(verbose_name='Data do Cadastro', blank=True, default=datetime.now)
    user = models.ForeignKey(User, verbose_name='Usuário')

    class Meta:
        ordering = ('-created_at',)
        verbose_name = u'Revista'
        verbose_name_plural = u'Revistas'

    def __unicode__(self):
        return self.number


def update_filename(instance, filename):
        path = "uploads/arquivos/"
        fname = filename.split('.')
        format = slugify(fname[0]) + ''.join([random.SystemRandom().choice(''.join(string.digits)) for i in range(8)]) + '.' + fname[-1]
        return os.path.join(path, format)


class File(models.Model):
    file = models.FileField('Arquivo', upload_to=update_filename)

    class Meta:
        verbose_name = u'Arquivo'
        verbose_name_plural = u'Arquivos'

    def __unicode__(self):
        return settings.MEDIA_URL + self.file.name


def video_pre_save(signal, instance, sender, **kwargs):
    if not instance.slug:
        instance.slug = slugify(instance.title)

def videocategory_pre_save(signal, instance, sender, **kwargs):
    if not instance.slug:
        instance.slug = slugify(instance.name)


signals.pre_save.connect(video_pre_save, sender=Video)
signals.pre_save.connect(videocategory_pre_save, sender=CategoryVideo)