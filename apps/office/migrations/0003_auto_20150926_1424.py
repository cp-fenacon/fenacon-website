# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('office', '0002_syndicate'),
    ]

    operations = [
        migrations.AddField(
            model_name='syndicate',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Data do Cadastro', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='syndicate',
            name='status',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='syndicate',
            name='user',
            field=models.ForeignKey(default=None, verbose_name=b'Usu\xc3\xa1rio', to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
    ]
