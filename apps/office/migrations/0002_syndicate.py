# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('office', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Syndicate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Exemplo: SESCON - Estado', max_length=150, verbose_name=b'Nome')),
                ('president', models.CharField(help_text=b'Exemplo: SESCON - Estado', max_length=150, verbose_name=b'Presidente')),
                ('address', models.CharField(max_length=150, verbose_name=b'Endere\xc3\xa7o')),
                ('city', models.CharField(max_length=100, verbose_name=b'Cidade')),
                ('state', models.CharField(max_length=50, verbose_name=b'Estado', choices=[(b'AC', b'Acre'), (b'AL', b'Alagoas'), (b'AP', b'Amap\xc3\xa1'), (b'AM', b'Amazonas'), (b'BA', b'Bahia'), (b'CE', b'Cear\xc3\xa1'), (b'DF', b'Distrito Federal'), (b'ES', b'Esp\xc3\xadrito Santo'), (b'GO', b'Goi\xc3\xa1s'), (b'MA', b'Maran\xc3\xa3o'), (b'MT', b'Mato Grosso'), (b'MS', b'Mato Grosso do Sul'), (b'MG', b'Minas Gerais'), (b'PA', b'Par\xc3\xa1'), (b'PB', b'Para\xc3\xadba'), (b'PR', b'Paran\xc3\xa1'), (b'PE', b'Pernanbuco'), (b'PI', b'Piau\xc3\xad'), (b'RJ', b'Rio de Janeiro'), (b'RN', b'Rio Grande do Norte'), (b'RS', b'Rio Grande do Sul'), (b'RO', b'Rond\xc3\xb4nia'), (b'RR', b'Roraima'), (b'SC', b'Santa Catarina'), (b'SP', b'S\xc3\xa3o Paulo'), (b'SE', b'Sergipe'), (b'TO', b'Tocantins')])),
                ('cep', models.CharField(max_length=50, verbose_name=b'CEP')),
                ('phone', models.CharField(max_length=50, verbose_name=b'Telefone')),
                ('email', models.EmailField(max_length=75)),
                ('page', models.URLField(null=True, verbose_name=b'Home page', blank=True)),
                ('code', models.CharField(max_length=100, verbose_name=b'C\xc3\xb3digo Sindical')),
            ],
            options={
                'verbose_name': 'Sindicato',
                'verbose_name_plural': 'Sindicatos',
            },
            bases=(models.Model,),
        ),
    ]
