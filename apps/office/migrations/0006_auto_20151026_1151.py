# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('office', '0005_auto_20150926_1838'),
    ]

    operations = [
        migrations.AlterField(
            model_name='syndicate',
            name='president',
            field=models.CharField(max_length=150, verbose_name=b'Presidente'),
        ),
    ]
