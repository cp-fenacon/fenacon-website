# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('office', '0006_auto_20151026_1151'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='filetoimport',
            name='user',
        ),
    ]
