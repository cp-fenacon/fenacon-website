# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('office', '0004_syndicate_region'),
    ]

    operations = [
        migrations.AlterField(
            model_name='syndicate',
            name='email',
            field=models.EmailField(max_length=254),
        ),
        migrations.AlterField(
            model_name='syndicate',
            name='region',
            field=models.CharField(max_length=50, verbose_name=b'Regi\xc3\xa3o', choices=[(b'norte', b'Norte'), (b'centro-oeste', b'Centro-Oeste'), (b'sul', b'Sul'), (b'nordeste', b'Nordeste'), (b'sudeste', b'Sudeste')]),
        ),
    ]
