# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('office', '0003_auto_20150926_1424'),
    ]

    operations = [
        migrations.AddField(
            model_name='syndicate',
            name='region',
            field=models.CharField(default=None, max_length=10, verbose_name=b'Regi\xc3\xa3o', choices=[(b'norte', b'Norte'), (b'centro-oeste', b'Centro-Oeste'), (b'sul', b'Sul'), (b'nordeste', b'Nordeste'), (b'sudeste', b'Sudeste')]),
            preserve_default=False,
        ),
    ]
