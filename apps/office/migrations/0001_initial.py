# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='FileToImport',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('file', models.FileField(upload_to=b'uploads/escritorios/', verbose_name=b'Arquivo')),
                ('created_at', models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Data do Cadastro', blank=True)),
                ('user', models.ForeignKey(verbose_name=b'Usu\xc3\xa1rio', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Arquivo para importa\xe7\xe3o',
                'verbose_name_plural': 'Arquivos para importa\xe7\xe3o',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Office',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cnpj', models.CharField(max_length=20)),
                ('company_name', models.CharField(max_length=150, verbose_name=b'Raz\xc3\xa3o social')),
                ('patio_type', models.CharField(max_length=150, null=True, verbose_name=b'Tipo de logradouro', blank=True)),
                ('address', models.CharField(max_length=150, verbose_name=b'Endere\xc3\xa7o')),
                ('number', models.CharField(max_length=10, null=True, verbose_name=b'N\xc3\xbamero', blank=True)),
                ('complement', models.CharField(max_length=150, null=True, verbose_name=b'Complemento', blank=True)),
                ('neighborhood', models.CharField(max_length=100, null=True, verbose_name=b'Bairro', blank=True)),
                ('city', models.CharField(max_length=100, null=True, verbose_name=b'Cidade', blank=True)),
                ('state', models.CharField(blank=True, max_length=50, null=True, verbose_name=b'Estado', choices=[(b'AC', b'Acre'), (b'AL', b'Alagoas'), (b'AP', b'Amap\xc3\xa1'), (b'AM', b'Amazonas'), (b'BA', b'Bahia'), (b'CE', b'Cear\xc3\xa1'), (b'DF', b'Distrito Federal'), (b'ES', b'Esp\xc3\xadrito Santo'), (b'GO', b'Goi\xc3\xa1s'), (b'MA', b'Maran\xc3\xa3o'), (b'MT', b'Mato Grosso'), (b'MS', b'Mato Grosso do Sul'), (b'MG', b'Minas Gerais'), (b'PA', b'Par\xc3\xa1'), (b'PB', b'Para\xc3\xadba'), (b'PR', b'Paran\xc3\xa1'), (b'PE', b'Pernanbuco'), (b'PI', b'Piau\xc3\xad'), (b'RJ', b'Rio de Janeiro'), (b'RN', b'Rio Grande do Norte'), (b'RS', b'Rio Grande do Sul'), (b'RO', b'Rond\xc3\xb4nia'), (b'RR', b'Roraima'), (b'SC', b'Santa Catarina'), (b'SP', b'S\xc3\xa3o Paulo'), (b'SE', b'Sergipe'), (b'TO', b'Tocantins')])),
                ('emails', models.CharField(max_length=200, null=True, blank=True)),
                ('phone', models.CharField(max_length=50, null=True, verbose_name=b'Telefone', blank=True)),
            ],
            options={
                'verbose_name': 'Escrit\xf3rio',
                'verbose_name_plural': 'Escrit\xf3rios',
            },
            bases=(models.Model,),
        ),
    ]
