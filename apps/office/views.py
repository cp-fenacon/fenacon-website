# coding=utf-8
from django.views.generic import ListView
from import_export import resources, fields
from apps.office.forms import FormSyndicate, FormOffice
from apps.office.models import Syndicate, Office


class SyndicateListView(ListView):
    model = Syndicate
    template_name = 'office/sindicatos.html'

    def get_context_data(self, **kwargs):
        context = super(SyndicateListView, self).get_context_data(**kwargs)
        context['form'] = FormSyndicate()
        return context

    def get_queryset(self):
        region = self.request.GET.get('region')
        state = self.request.GET.get('state')
        object_list = Syndicate.objects.filter(status=True).order_by('state')
        if region:
            object_list = object_list.filter(region=region)
        if state:
            object_list = object_list.filter(state=state)
        return object_list


class OfficeListView(ListView):
    model = Office
    template_name = 'office/escritorios.html'
    paginate_by = 8

    def get_context_data(self, **kwargs):
        context = super(OfficeListView, self).get_context_data(**kwargs)
        context['form'] = FormOffice()
        return context

    def get_queryset(self):
        city = self.request.GET.get('city')
        neighborhood = self.request.GET.get('neighborhood')
        state = self.request.GET.get('state')
        object_list = Office.objects.none()
        if city or neighborhood or state:
            object_list = Office.objects.all().order_by('state')
            if city:
                object_list = object_list.filter(city__icontains=city)
            if neighborhood:
                object_list = object_list.filter(neighborhood__icontains=neighborhood)
            if state:
                object_list = object_list.filter(state=state)
        return object_list


class OfficeResource(resources.ModelResource):
    # cnpj = fields.Field(column_name='CNPJ')
    # company_name = fields.Field(column_name='Razão')
    # patio_type = fields.Field(column_name='Tipo Logadouro')
    # address = fields.Field(column_name='Endereço')
    # number = fields.Field(column_name='Número')
    # complement = fields.Field(column_name='Complemento')
    # neighborhood = fields.Field(column_name='Bairro')
    # city = fields.Field(column_name='Cidade')
    # state = fields.Field(column_name='Estado')
    # emails = fields.Field(column_name='Email')
    # phone = fields.Field(column_name='Telefone')

    class Meta:
        model = Office
        # import_id_fields = ['id']
        # fields = ('cnpj', 'company_name', 'patio_type', 'address', 'number', 'complement', 'neighborhood', 'city',
        #           'state', 'emails', 'phone')

    def get_instance(self, instance_loader, row):
        return False

