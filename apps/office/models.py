# coding=utf-8
from datetime import datetime
from csvImporter.model import CsvDbModel
from django.db import models
from apps.accounts.models import User
from django.db.models.signals import post_save

UF_CHOICES = (
    ('AC', 'Acre'),
    ('AL', 'Alagoas'),
    ('AP', 'Amapá'),
    ('AM', 'Amazonas'),
    ('BA', 'Bahia'),
    ('CE', 'Ceará'),
    ('DF', 'Distrito Federal'),
    ('ES', 'Espírito Santo'),
    ('GO', 'Goiás'),
    ('MA', 'Maranhão'),
    ('MT', 'Mato Grosso'),
    ('MS', 'Mato Grosso do Sul'),
    ('MG', 'Minas Gerais'),
    ('PA', 'Pará'),
    ('PB', 'Paraíba'),
    ('PR', 'Paraná'),
    ('PE', 'Pernambuco'),
    ('PI', 'Piauí'),
    ('RJ', 'Rio de Janeiro'),
    ('RN', 'Rio Grande do Norte'),
    ('RS', 'Rio Grande do Sul'),
    ('RO', 'Rondônia'),
    ('RR', 'Roraima'),
    ('SC', 'Santa Catarina'),
    ('SP', 'São Paulo'),
    ('SE', 'Sergipe'),
    ('TO', 'Tocantins')
)

REGION = {('norte', 'Norte'),
          ('nordeste', 'Nordeste'),
          ('centro-oeste', 'Centro-Oeste'),
          ('sudeste', 'Sudeste'),
          ('sul', 'Sul')}


class Syndicate(models.Model):
    name = models.CharField('Nome', max_length=150, help_text="Exemplo: SESCON - Estado")
    president = models.CharField('Presidente', max_length=150)
    address = models.CharField('Endereço', max_length=150)
    region = models.CharField('Região', max_length=50, choices=REGION)
    state = models.CharField('Estado', max_length=50, choices=UF_CHOICES)
    city = models.CharField('Cidade', max_length=100)
    cep = models.CharField('CEP', max_length=50)
    phone = models.CharField('Telefone', max_length=50)
    email = models.EmailField()
    page = models.URLField("Home page", blank=True, null=True)
    code = models.CharField("Código Sindical", max_length=100)
    status = models.BooleanField(default=True)
    created_at = models.DateTimeField(verbose_name='Data do Cadastro', blank=True, default=datetime.now)
    user = models.ForeignKey(User, verbose_name='Usuário')

    class Meta:
        verbose_name = u'Sindicato'
        verbose_name_plural = u'Sindicatos'

    def __unicode__(self):
        return self.name


class Office(models.Model):
    cnpj = models.CharField(max_length=20)
    company_name = models.CharField('Razão social', max_length=150)
    patio_type = models.CharField('Tipo de logradouro', max_length=150, blank=True, null=True)
    address = models.CharField('Endereço', max_length=150)
    number = models.CharField('Número', max_length=10, blank=True, null=True)
    complement = models.CharField('Complemento', max_length=150, blank=True, null=True)
    neighborhood = models.CharField('Bairro', max_length=100, blank=True, null=True)
    city = models.CharField('Cidade', max_length=100, blank=True, null=True)
    state = models.CharField('Estado', max_length=50, choices=UF_CHOICES, blank=True, null=True)
    cep = models.CharField(max_length=200, blank=True, null=True)
    emails = models.CharField(max_length=200, blank=True, null=True)
    phone = models.CharField('Telefone', max_length=50, blank=True, null=True)

    class Meta:
        verbose_name = u'Escritório'
        verbose_name_plural = u'Escritórios'

    def __unicode__(self):
        return self.company_name


class OfficeCsvModel(CsvDbModel):

    class Meta:
        delimiter = ";"
        dbModel = Office


class FileToImport(models.Model):
    file = models.FileField('Arquivo', upload_to='uploads/escritorios/')
    created_at = models.DateTimeField(verbose_name='Data do Cadastro', blank=True, default=datetime.now)

    class Meta:
        verbose_name = u'Arquivo para importação'
        verbose_name_plural = u'Arquivos para importação'

    def __unicode__(self):
        return u'%s' % str(self.file).split('/')[-1]


def crete_offices(sender, instance, **kwargs):
    from django.conf import settings
    office_csv_list = OfficeCsvModel.import_data(data=open(settings.MEDIA_ROOT + '/' + str(instance.file)))
    for row in range(0, len(office_csv_list)):
        office = Office()
        office.cnpj = office_csv_list[row].cnpj
        office.company_name = office_csv_list[row].company_name
        office.patio_type = office_csv_list[row].patio_type
        office.address = office_csv_list[row].address
        office.number = office_csv_list[row].number
        office.complement = office_csv_list[row].complement
        office.neighborhood = office_csv_list[row].neighborhood
        office.city = office_csv_list[row].city
        office.state = office_csv_list[row].state
        office.cep = office_csv_list[row].cep
        office.emails = office_csv_list[row].emails
        office.phone = office_csv_list[row].phone
# register the signal
post_save.connect(crete_offices, sender=FileToImport)