# -*- coding: utf-8 -*-
from ckeditor.widgets import CKEditorWidget
from django import forms
from django.core.mail import send_mail
from apps.website.models import News, Event

UF_CHOICES = (
    ('', 'Selecione um estado'),
    ('AC', 'Acre'),
    ('AL', 'Alagoas'),
    ('AP', 'Amapá'),
    ('AM', 'Amazonas'),
    ('BA', 'Bahia'),
    ('CE', 'Ceará'),
    ('DF', 'Distrito Federal'),
    ('ES', 'Espírito Santo'),
    ('GO', 'Goiás'),
    ('MA', 'Maranhão'),
    ('MT', 'Mato Grosso'),
    ('MS', 'Mato Grosso do Sul'),
    ('MG', 'Minas Gerais'),
    ('PA', 'Pará'),
    ('PB', 'Paraíba'),
    ('PR', 'Paraná'),
    ('PE', 'Pernambuco'),
    ('PI', 'Piauí'),
    ('RJ', 'Rio de Janeiro'),
    ('RN', 'Rio Grande do Norte'),
    ('RS', 'Rio Grande do Sul'),
    ('RO', 'Rondônia'),
    ('RR', 'Roraima'),
    ('SC', 'Santa Catarina'),
    ('SP', 'São Paulo'),
    ('SE', 'Sergipe'),
    ('TO', 'Tocantins')
)

REGION = {
    ('', 'Selecione uma região'),
    ('norte', 'Norte'),
    ('nordeste', 'Nordeste'),
    ('centro-oeste', 'Centro-Oeste'),
    ('sudeste', 'Sudeste'),
    ('sul', 'Sul')
}




class FormState(forms.Form):
    state = forms.ChoiceField(choices=sorted(UF_CHOICES, key=lambda x: x[0]), required=True)


class FormSyndicate(forms.Form):
    region = forms.ChoiceField(choices=sorted(REGION, key=lambda x: x[0]), required=True)
    state = forms.ChoiceField(choices=sorted(UF_CHOICES, key=lambda x: x[0]), required=True)


class FormOffice(forms.Form):
    state = forms.ChoiceField(choices=sorted(UF_CHOICES, key=lambda x: x[0]), required=True)
    city = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Cidade'}))
    neighborhood = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Bairro'}))
