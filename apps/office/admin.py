# coding=utf-8
from django.contrib import admin
from apps.office.models import Office, FileToImport, Syndicate


class OfficeAdmin(admin.ModelAdmin):
    list_display = ('company_name', 'cnpj', 'state', 'city', 'neighborhood', 'cep', 'phone', 'emails')
    list_filter = ['state']
    search_fields = ['company_name', 'city', 'address', 'cnpj']
    fieldsets = [
        (None, {'fields': ['cnpj', 'company_name']}),
        (u'Endereço', {'fields': ['patio_type', 'address', 'number', 'complement', 'neighborhood', 'state', 'city']}),
        (u'Informações', {'fields': ['phone', 'emails']}),
    ]


class SyndicateAdmin(admin.ModelAdmin):
    list_display = ('name', 'president', 'user', 'created_at', 'status')
    list_filter = ['region', 'state', 'status']
    search_fields = ['name', 'president', 'code', 'email', 'phone', 'page', 'cep', 'city']
    fieldsets = [
        (None, {'fields': ['name', 'president']}),
        (u'Endereço', {'fields': ['address', 'region', 'state', 'city']}),
        (u'Informações', {'fields': ['code', 'cep', 'phone', 'email', 'page', 'status']}),
    ]

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()


admin.site.register(Syndicate, SyndicateAdmin)
admin.site.register(Office, OfficeAdmin)
# admin.site.register(FileToImport)
